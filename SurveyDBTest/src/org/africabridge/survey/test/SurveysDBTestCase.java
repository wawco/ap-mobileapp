package org.africabridge.survey.test;

import org.africabridge.core.Answer;
import org.africabridge.core.AnswerList;
import org.africabridge.core.QuestionList;
import org.africabridge.data.Surveys;
import org.w3c.dom.Node;

import android.test.AndroidTestCase;

public class SurveysDBTestCase extends AndroidTestCase {
	AnswerList originalAnswers;
	QuestionList questionList;
	Node node;
	
	int sid1, sid2, sid3, sid4;
	int qid1, qid2, qid3, qid4;
	int iid1, iid2, iid3, iid4;
	String val1, val2, val3, val4;
	Boolean skip1, skip2, skip3, skip4;
	Boolean valid1, valid2, valid3, valid4;
	
	@Override
	protected void setUp() throws Exception {
		// TODO Auto-generated method stub
		super.setUp();
		originalAnswers = new AnswerList(null);
		questionList = new QuestionList(null);
	}
	
	public void testSaveSingleHeaderAnswerLoadSameAnswer() {
		Answer anAnswer;
		//Surveys.saveHeaderAnswer(sid1, qid1, val1, skip1, valid1);
		AnswerList answers = Surveys.getHeaderAnswers(sid1);
		//anAnswer = answers.get
		//assertEquals(originalAnswer,answers);
		fail("This test required additional implementation.");
	}
	
	public void testSaveSingleItemLoadSameItem() {
		//Surveys.saveItemAnswer(sid1, iid1, qid1, val1, skip1, valid1);
		fail("This test required additional implementation.");
	}
}
