package org.africabridge.survey.test;

import org.africabridge.survey.Home;

import com.jayway.android.robotium.solo.Solo;

import android.test.ActivityInstrumentationTestCase2;

public class LanguageChoiceRevertVerifyTest extends
        ActivityInstrumentationTestCase2<Home> {
    
    private Solo solo;
    
    public LanguageChoiceRevertVerifyTest() {
        super(Home.class);
    }

    public LanguageChoiceRevertVerifyTest(Class<Home> activityClass) {
        super(activityClass);
    }

    protected void setUp() throws Exception {
        super.setUp();
        solo = new Solo(getInstrumentation(), getActivity());
    }

    public void testLanguageSettingsRetentionSwitch3() throws Exception {
        assertTrue("Wrong initial language setting.", solo.searchText("Kaya"));
        solo.sendKey(Solo.MENU);
        solo.clickOnText("Mazingira");
        solo.pressSpinnerItem(0, -1);
        solo.clickOnButton("Kuokoa");
        solo.sendKey(Solo.MENU);
        assertTrue(solo.searchText("Settings"));
        assertTrue(solo.searchText("Export Data"));
    }
    
    
    protected void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
    }

}
