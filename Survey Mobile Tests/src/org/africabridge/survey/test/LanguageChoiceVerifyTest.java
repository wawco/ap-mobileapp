package org.africabridge.survey.test;

import org.africabridge.survey.Home;

import com.jayway.android.robotium.solo.Solo;

import android.test.ActivityInstrumentationTestCase2;

public class LanguageChoiceVerifyTest extends
        ActivityInstrumentationTestCase2<Home> {
    
    private Solo solo;
    
    public LanguageChoiceVerifyTest() {
        super(Home.class);
    }

    public LanguageChoiceVerifyTest(Class<Home> activityClass) {
        super(activityClass);
    }

    protected void setUp() throws Exception {
        super.setUp();
        solo = new Solo(getInstrumentation(), getActivity());
    }

    public void testLanguageSettingsRetentionSwitch1() throws Exception {
        assertTrue("Wrong initial language setting.", solo.searchText("Household"));
        solo.sendKey(Solo.MENU);
        solo.clickOnText("Settings");
        solo.pressSpinnerItem(0, 1);
        solo.clickOnButton("Save");
        assertTrue("Home screen title doesn't reflect language change.",solo.searchText("Kaya"));
        solo.sendKey(Solo.MENU);
        assertTrue("Settings menu doesn't reflect language change.", solo.searchText("Mazingira"));
    }
    
    
    protected void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
    }

}
