package org.africabridge.survey.test;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class LanguageTests extends TestCase {

    public static Test suite() {
        TestSuite suite = new TestSuite(LanguageTests.class.getName());
        //$JUnit-BEGIN$
        suite.addTestSuite(LanguageChoiceVerifyTest.class);
        suite.addTestSuite(ApplicationRestartTests.class);
        suite.addTestSuite(LanguageChoiceRetentionTest.class);
        suite.addTestSuite(LanguageChoiceRevertVerifyTest.class);
        //$JUnit-END$
        return suite;
    }
    
}
