package org.africabridge.survey.test;

import org.africabridge.survey.Home;

import com.jayway.android.robotium.solo.Solo;

import android.test.ActivityInstrumentationTestCase2;

public class LanguageChoiceRetentionTest extends
        ActivityInstrumentationTestCase2<Home> {
    
    private Solo solo;

    public LanguageChoiceRetentionTest() {
        super(Home.class);
    }
    
    public LanguageChoiceRetentionTest(Class<Home> activityClass) {
        super(activityClass);
    }

    protected void setUp() throws Exception {
        super.setUp();
        solo = new Solo(getInstrumentation(), getActivity());
    }

    public void testLanguageSettingsRetentionSwitch2() throws Exception {
        assertTrue("Home screen language not expected language.", solo.searchText("Kaya"));
        solo.sendKey(Solo.MENU);
        assertTrue("Home screen language not expected language.", solo.searchText("Mazingira"));
        assertTrue("Home screen language not expected language.", solo.searchText("Export Data"));
    }
    
    protected void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
    }

}
