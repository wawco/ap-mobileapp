package org.africabridge.survey.test;

import junit.framework.Assert;
import android.app.Activity;
import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import org.africabridge.survey.*;

import com.jayway.android.robotium.solo.Solo;


public class EditorTest extends ActivityInstrumentationTestCase2<Home> {

    private Solo solo;
    private Activity theActivity;
    
    public EditorTest() {
        super(Home.class);
    }
    
    public EditorTest(Class<Home> activityClass) {
        super(activityClass);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void setUp() throws Exception {
        super.setUp();
        theActivity = getActivity();
        solo = new Solo(getInstrumentation(), theActivity);
    }
     
    
    /** All three testLanguageSettingsRetentionSwitch() tests need to be run
     *  in an explicit order.  This might take fiddling depending on your environment.
     *  TODO: Put each one into a separate test class and run them in a Suite
     */
      
    
    
    @Override
    public void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
    }


}
