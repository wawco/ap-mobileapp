package org.africabridge.survey.test;

import org.africabridge.survey.SurveyApplication;

import com.jayway.android.robotium.solo.Solo;

import android.content.Context;
import android.test.ApplicationTestCase;

public class ApplicationRestartTests extends
        ApplicationTestCase<SurveyApplication> {
    private Context context;

    public ApplicationRestartTests() {
        super(SurveyApplication.class);
    }
    public ApplicationRestartTests(Class<SurveyApplication> appClass) {
        super(appClass);
    }

    protected void setUp() throws Exception {
        super.setUp();
        createApplication();
        context = this.getContext();
    }
    
    public void testDoNadda() {
        
    }

    protected void tearDown() throws Exception {
        super.tearDown();
    }

}
