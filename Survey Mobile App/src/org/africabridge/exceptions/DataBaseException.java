package org.africabridge.exceptions;

public class DataBaseException extends Exception {

	public DataBaseException() {
		// TODO Auto-generated constructor stub
	}

	public DataBaseException(String detailMessage) {
		super(detailMessage);
		// TODO Auto-generated constructor stub
	}

	public DataBaseException(Throwable throwable) {
		super(throwable);
		// TODO Auto-generated constructor stub
	}

	public DataBaseException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
		// TODO Auto-generated constructor stub
	}

}
