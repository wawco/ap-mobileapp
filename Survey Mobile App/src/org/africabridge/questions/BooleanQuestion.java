package org.africabridge.questions;

import java.security.spec.InvalidParameterSpecException;

import org.africabridge.exceptions.ValidationException;
import org.w3c.dom.Node;

/**
 * Question expecting a True or False answer.
 * @author ycroosh
 *
 */
public class BooleanQuestion extends QuestionBase {

	protected BooleanQuestion(Node node) throws NumberFormatException,
			InvalidParameterSpecException {
		super(node);
	}

	@Override
	public String AcceptAnswer(String answer) throws ValidationException {

		Boolean value = Boolean.parseBoolean(answer);

		return getJump(value.toString());

	}

}
