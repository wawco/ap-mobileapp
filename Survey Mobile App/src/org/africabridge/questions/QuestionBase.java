package org.africabridge.questions;

import java.security.InvalidParameterException;
import java.security.spec.InvalidParameterSpecException;
import java.util.HashMap;
import java.util.Map;

import org.africabridge.core.*;
import org.africabridge.exceptions.ValidationException;
import org.w3c.dom.*;

/**
 * Provides basic functionality for the inherited question types,
 * such as common field parsing, default answer validation etc.
 * @author ycroosh
 *
 */
public abstract class QuestionBase extends Caption {

	private enum QuestionTypes {
		TEXT, NUMERIC, BOOLEAN
	}

	protected int id;
	protected String code;
	protected String property;

	protected boolean isOptional;
	protected boolean isCritical;
	protected boolean isPrimaryKey;
	protected int maxAnswers = 1;
	
	protected final Map<String, String> jumps = new HashMap<String, String>();
	protected String validationExpression = null;
	
	public int id() {
		return id;
	}


	/**
	 * Question column code defined on AB's form
	 * @return string
	 */
	public String code() {
		return code;
	}

	/**
	 * Property name in the deskto application 
	 * @return string
	 */
	public String propertyName() {
		return property;
	}

	/**
	 * Determines if the question can be skipped without the "nag screen"
	 * @return
	 */
	public boolean isOptional() {
		return isOptional && !isCritical;
	}

	/**
	 * Determines if the question can be skipped
	 * If true, the survey should not advance to the next group until the answer is provided
	 * @return
	 */
	public boolean isCritical() {
		return isCritical;
	}
	
	/**
	 * If true, the answer will be used as a primary key for the survey section
	 * @return
	 */
	public boolean isPrimaryKey() {
		return isPrimaryKey;
	}
	

	/**
	 * Maximum number of answers expected for this questions
	 * NOTE: The question will accept from 1 - to maxAnswers number of answers
	 * @return
	 */
	public int maxAnswers() {
		return maxAnswers;
	}





	public String getCaption(Cultures culture) {

		if (captions.containsKey(culture))
			return captions.get(culture);

		return null;

	}

	/**
	 * Validates the provided answer and if accepted returns an (optional)
	 * jump-to label.
	 * 
	 * @param answer
	 * @return
	 * @throws ValidationException
	 */
	public String AcceptAnswer(String answer) throws ValidationException {
		if ((answer == null || answer.length()==0) && !isOptional)
			throw new ValidationException("This questoin can't be skipped");

		if (validationExpression != null && answer != null
				&& !answer.matches(validationExpression))
			throw new ValidationException("This is not a valid answer");
		
		return getJump(answer);
	}
	
	
	protected String getJump(String answer)
	{
		if (jumps.containsKey(answer.toLowerCase()))
			return jumps.get(answer.toLowerCase());

		return null;
	}

	/* Constructors/Builders */

	/**
	 * Creates an instance of a QuestionBase from the XML node
	 * using a factory approach
	 * @param source
	 *            - "question" node from the survey template
	 * @return Instance of one of the QuestionBase subclasses
	 * @throws InvalidParameterSpecException
	 * @throws IllegalArgumentException
	 */
	public static QuestionBase CreateInstance(Node source)
			throws InvalidParameterSpecException, IllegalArgumentException {

		// first step is to determine what type of question this is

		NamedNodeMap attributes = source.getAttributes();

		// get string value of the type attribute
		String typeAttr = attributes.getNamedItem("type").getNodeValue();

		// convert to the enum value (does validation as well)
		QuestionTypes type = (QuestionTypes) Enum.valueOf(QuestionTypes.class,
				typeAttr);

		// depending on the type, call CreateInstance on one of the subclasses.
		switch (type) {
		case TEXT: {
			return TextQuestion.CreateInstance(source);
		}
		case NUMERIC: {
			return NumericQuestion.CreateInstance(source);
		}
		case BOOLEAN: {
			return new BooleanQuestion(source);
		}
		}

		// if we made it here, an unexpected type must've been encountered
		throw new InvalidParameterException(
				"Unexpected question type encountered");
	}

	
	protected QuestionBase(Node source) throws NumberFormatException,
			InvalidParameterSpecException {

		NamedNodeMap attributes = source.getAttributes();

		// parse the ID (the attribute is required, so it will newer be null
		this.id = Integer
				.parseInt(attributes.getNamedItem("id").getNodeValue());

		// the rest of the attributes are optional

		// parse property
		Node temp = attributes.getNamedItem("property");

		if (temp != null)
			this.property = temp.getNodeValue();

		// parse code
		temp = attributes.getNamedItem("code");

		if (temp != null)
			this.code = temp.getNodeValue();

		// parse "isRequired"
		temp = attributes.getNamedItem("isOptional");

		if (temp != null)
			this.isOptional = Boolean.parseBoolean(temp.getNodeValue());

		// parse "isCritical"
		temp = attributes.getNamedItem("isCritical");

		if (temp != null)
			this.isCritical = Boolean.parseBoolean(temp.getNodeValue());


		// parse "isRequired"
		temp = attributes.getNamedItem("isPrimaryKey");

		if (temp != null)
		{
			this.isPrimaryKey = Boolean.parseBoolean(temp.getNodeValue());
		
			if(this.isPrimaryKey && !(this instanceof NumericQuestion))
				throw new IllegalArgumentException("Only numeric questions can be primary keys");
		}

		
		
		// parse "masAnswers"
		temp = attributes.getNamedItem("maxAnswers");

		if (temp != null)
			this.maxAnswers = Integer.parseInt(temp.getNodeValue());

		this.loadCaptions(XmlHelper.getChildNode(source, "caption"));

		this.loadJumps(XmlHelper.getChildNode(source, "jumps"));

	}


	private void loadJumps(Node jumpsNode)
			throws InvalidParameterSpecException, DOMException {
		if (jumpsNode == null)
			return;

		Node node = jumpsNode.getFirstChild();

		while (node != null) {
			if (node.getNodeName().equalsIgnoreCase("jump")) {
				addJump((Element) node);
			}
			node = node.getNextSibling();
		}
	}

	private void addJump(Element jump) throws InvalidParameterSpecException,
			DOMException {
		String destination = jump.getTextContent();

		String value = jump.getAttribute("value");

		this.jumps.put(value.trim(), destination.trim());
	}

}
