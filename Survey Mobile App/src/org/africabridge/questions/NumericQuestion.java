package org.africabridge.questions;

import java.security.spec.InvalidParameterSpecException;

import org.africabridge.exceptions.ValidationException;
import org.w3c.dom.Node;

/**
 * Question that expects a numeric answer
 * @author Team D
 *
 */
public class NumericQuestion extends QuestionBase {

	protected NumericQuestion(Node node) throws NumberFormatException,
			InvalidParameterSpecException {
		super(node);
	}

	private Float minValue = null;
	private Float maxValue = null;
	private Boolean allowDecimal = false;

	public Float getMinValue() {
		return minValue;
	}

	public Float getMaxValue() {
		return maxValue;
	}

	public Boolean getAllowDecimal() {
		return allowDecimal;
	}

	@Override
	public String AcceptAnswer(String answer) throws ValidationException {

		try {
			Float value = Float.parseFloat(answer);

			if (minValue != null && value < minValue) {
				throw new ValidationException();
			}

			if (maxValue != null && value > maxValue) {
				throw new ValidationException();
			}

			
		} catch (NumberFormatException e) {
			if (!isOptional)
				throw new ValidationException();
		}
		
		return null;
	}

	public static QuestionBase CreateInstance(Node node)
			throws NumberFormatException, InvalidParameterSpecException {
		/*
		 * //NodeList options = doc.getNodeList("options/option", node);
		 * 
		 * if(options!=null && options.getLength()>0) { return new
		 * NumericSelectQuestion(node); } else { return new
		 * NumericQuestion(node); }
		 */
		return new NumericQuestion(node);
	}
}
