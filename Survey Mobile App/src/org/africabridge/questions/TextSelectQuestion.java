package org.africabridge.questions;

import java.security.spec.InvalidParameterSpecException;
import java.util.HashMap;
import java.util.Map;

import org.africabridge.core.*;
import org.africabridge.exceptions.ValidationException;
import org.w3c.dom.*;

import android.util.Log;

public class TextSelectQuestion extends TextQuestion {

	protected TextSelectQuestion(Node source) throws NumberFormatException,
			InvalidParameterSpecException {
		super(source);

		this.loadOptions(XmlHelper.getChildNode(source, "options"));
	}

	private final Map<Cultures, Map<Integer, String>> options = new HashMap<Cultures, Map<Integer, String>>();
	private final Map<Cultures, Map<String, Integer>> reverseLookupMap = new HashMap<Cultures, Map<String, Integer>>();

	@Override
	public String AcceptAnswer(String answer) throws ValidationException {

		int id = Integer.parseInt(answer);

		for (Map<Integer, String> map : options.values()) {
			if (map.containsKey(id))
				return super.AcceptAnswer(answer);
		}

		throw new ValidationException("This optiond doesn't exist");
	}

	public String[] getOptions(Cultures culture) {
		String[] array = new String[0];

		return options.get(culture).values().toArray(array);
	}

	public int getOptionId(String value, Cultures culture) {
		if (reverseLookupMap.containsKey(culture)
				&& reverseLookupMap.get(culture).containsKey(
						value.toLowerCase())) {
			return reverseLookupMap.get(culture).get(value.toLowerCase());
		}

		return -1;
	}

	public String getOption(Cultures culture, int id) {
		if (!options.containsKey(culture)
				|| !options.get(culture).containsKey(id))
			return null;

		return options.get(culture).get(id);
	}

	public String getOption(Cultures culture, String id) {
		try {

			return getOption(culture, Integer.parseInt(id));
		} catch (Exception e) {
			Log.getStackTraceString(e);
			return null;
		}
	}

	private void loadOptions(Node optionNode)
			throws InvalidParameterSpecException, DOMException {
		if (optionNode == null)
			return;

		// add all cultures to the map
		this.options.put(Cultures.English, new HashMap<Integer, String>());
		this.options.put(Cultures.Swahili, new HashMap<Integer, String>());

		this.reverseLookupMap.put(Cultures.English,
				new HashMap<String, Integer>());
		this.reverseLookupMap.put(Cultures.Swahili,
				new HashMap<String, Integer>());

		Node node = optionNode.getFirstChild();

		while (node != null) {
			if (node.getNodeName().equalsIgnoreCase("option")) {
				addOption((Element) node);
			}
			node = node.getNextSibling();
		}
	}

	private void addOption(Element option)
			throws InvalidParameterSpecException, DOMException {
		int id = Integer.parseInt(option.getAttribute("id"));

		Node node = option.getFirstChild();

		while (node != null) {
			if (node.getNodeType() == 1
					&& node.getNodeName().equalsIgnoreCase("text")) {
				addOptionString(id, (Element) node);
			}
			node = node.getNextSibling();
		}
	}

	private void addOptionString(int optionId, Element string)
			throws InvalidParameterSpecException, DOMException {
		String value = string.getTextContent().trim();

		Cultures culture = Cultures.parse(string.getAttribute("culture"));

		this.options.get(culture).put(optionId, value);

		value = value.toLowerCase();

		if (reverseLookupMap.get(culture).containsKey(value)
				&& reverseLookupMap.get(culture).get(value).equals(optionId)) {
			throw new IllegalArgumentException("Duplicate strings encoutenred");
		}

		reverseLookupMap.get(culture).put(value, optionId);
	}
}
