package org.africabridge.questions;

import java.security.spec.InvalidParameterSpecException;

import org.africabridge.core.XmlHelper;
import org.africabridge.exceptions.ValidationException;
import org.w3c.dom.Node;

public class TextQuestion extends QuestionBase {

	protected TextQuestion(Node node) throws NumberFormatException, InvalidParameterSpecException {
		super(node);
	}

	@Override
	public String AcceptAnswer(String answer) throws ValidationException {
		return super.AcceptAnswer(answer);
	}
	
	public static QuestionBase CreateInstance(Node source) throws NumberFormatException, InvalidParameterSpecException
	{

		if(XmlHelper.hasChildNode(source, "options"))
			return new TextSelectQuestion(source);
		
		return new TextQuestion(source);
	}
}
