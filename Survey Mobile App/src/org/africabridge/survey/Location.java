package org.africabridge.survey;

import java.util.Arrays;

import org.africabridge.core.Survey;
import org.africabridge.world.LocationNameComparator;
import org.africabridge.world.Village;
import org.africabridge.world.Ward;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class Location extends FragmentActivity {

	// This should be the code to execute upon creation of the class.
	// Probably lots of initialization type stuff.
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		// no context menu on "utility" activities.

		setContentView(R.layout.activity_location);

		Spinner spinner = (Spinner) this.findViewById(R.id.wardSpinner);

		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

				 Ward ward = (Ward)((Spinner) Location.this.findViewById(R.id.wardSpinner)).getAdapter().getItem(position);
				 bindVillages(ward);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});
	}

	public void saveLocation(View view) {

		Ward ward = (Ward)((Spinner)this.findViewById(R.id.wardSpinner)).getSelectedItem();
		
		Village village = (Village)((Spinner)this.findViewById(R.id.villageSpinner)).getSelectedItem();
		String subVillage = ((EditText)this.findViewById(R.id.subVillageEdit)).getText().toString();
		
		Survey.setLocation(ward, village, subVillage);
		
		this.finishActivity(RESULT_OK);
		this.finish();
	}

	@Override
	public void onPause() {
		super.onPause();

		/*
		 * SharedPreferences.Editor edit = prefs.edit();
		 * 
		 * edit.putString("world_council",
		 * this.councilSettingView.getText().toString());
		 * edit.putString("world_ward",
		 * this.wardSettingView.getText().toString());
		 * edit.putString("world_village",
		 * this.villageSettingView.getText().toString());
		 * 
		 * edit.commit();
		 */
		this.finish();
	}

	@Override
	public void onResume() {
		super.onResume();
		bindWards();
		Ward ward = Survey.ward();

		if (ward != null) {
			bindVillages(ward);
			Spinner spinner = (Spinner) this.findViewById(R.id.wardSpinner);

			ArrayAdapter<Ward> adapter = (ArrayAdapter<Ward>) spinner.getAdapter();

			spinner.setSelection(adapter.getPosition(ward));
		}
		
		((EditText)this.findViewById(R.id.subVillageEdit)).setText(Survey.subVillage());
	}

	private void bindWards() {
		Spinner spinner = (Spinner) this.findViewById(R.id.wardSpinner);

		Ward[] wards =   Survey.district().getWards().toArray(new Ward[0]);
		
		//sort the array so ward names are in alphabetical order
		Arrays.sort(wards, new LocationNameComparator());
		
		ArrayAdapter<Ward> adapter = new ArrayAdapter<Ward>(this,
				android.R.layout.simple_spinner_item, wards);

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner.setAdapter(adapter);
	}

	private void bindVillages(Ward ward) {
		Spinner spinner = (Spinner) this.findViewById(R.id.villageSpinner);

		//sort the array so village names are in alphabetic order
		Village[] villages = ward.getVillages().toArray(new Village[0]);
		
		Arrays.sort(villages,new LocationNameComparator());
		
		ArrayAdapter<Village> adapter = new ArrayAdapter<Village>(this,
				android.R.layout.simple_spinner_item, villages);

		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		spinner.setAdapter(adapter);
		
		Village village = Survey.village();

		if (village != null) {
			spinner.setSelection(adapter.getPosition(village));
		}
	}
}
