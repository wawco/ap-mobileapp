package org.africabridge.survey;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import com.wawco.android.widget.*;

import javax.xml.datatype.Duration;

import org.africabridge.core.Answer;
import org.africabridge.core.AnswerList;
import org.africabridge.core.BundleKeys;
import org.africabridge.core.Cultures;

import org.africabridge.core.ItemSummary;
import org.africabridge.core.QuestionGroup;
import org.africabridge.core.Survey;
import org.africabridge.core.SurveySummary;
import org.africabridge.data.Surveys;
import org.africabridge.exceptions.InvalidStateException;
import org.africabridge.exceptions.ValidationException;
import org.africabridge.questions.BooleanQuestion;
import org.africabridge.questions.NumericQuestion;
import org.africabridge.questions.QuestionBase;
import org.africabridge.questions.TextQuestion;
import org.africabridge.questions.TextSelectQuestion;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class QuestionPresenter extends FragmentActivity {

	private long surveyId;
	private long itemId;
	private AnswerList answerList;

	String jumpTo = null;
	// private final Map<View, QuestionBase> controlMap = new HashMap<View,
	// QuestionBase>();
	private String mode;

	private static final String TAG = "QuestionPresenter";

	public static class Modes {

		public final static String UNKNOWN = "";
		public final static String HEADER = "header";
		public final static String ITEM = "item";

		public static String parse(String value) {

			if (value.equalsIgnoreCase("header"))
				return HEADER;

			if (value.equalsIgnoreCase("item"))
				return ITEM;

			return UNKNOWN;
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		OptionMenuFragment.attachOptionMenu(getSupportFragmentManager());

		try {

			// ID's are passed via the bundle
			Bundle b = getIntent().getExtras();

			if (b == null || !b.containsKey(BundleKeys.MODE)) {
				throw new IllegalArgumentException(
						"Presenter mode not passed or the bundle is missing");
			}

			mode = Modes.parse(b.getString(BundleKeys.MODE));

			if (mode == Modes.HEADER) {
				// header can have ID
				surveyId = b.containsKey(BundleKeys.SURVEY_ID) ? b
						.getLong(BundleKeys.SURVEY_ID) : -1;

				answerList = surveyId > 0 ? Surveys.getHeaderAnswers(surveyId)
						: new AnswerList(Survey.getTemplate().headerQuestions());
			} else if (mode == Modes.ITEM) {
				// item HAS to have survey ID
				if (!b.containsKey(BundleKeys.SURVEY_ID))
					throw new IllegalArgumentException("Survey ID is missing");

				surveyId = b.getLong(BundleKeys.SURVEY_ID);

				itemId = b.containsKey(BundleKeys.ITEM_ID) ? b
						.getLong(BundleKeys.ITEM_ID) : -1;

				answerList = itemId > 0 ? Surveys.getItemAnswers(surveyId,
						itemId) : new AnswerList(Survey.getTemplate()
						.itemQuestions());
			} else {
				throw new IllegalArgumentException("Unrecognized survey mode");
			}

		} catch (Exception e) {
			// log the exception
			Log.e(TAG, Log.getStackTraceString(e));

			// tell the user someting went wrong and finish the activity
			Toast.makeText(
					this,
					getResources().getString(R.string.survey_genericError)
							+ "\n" + e.getClass().getSimpleName() + ": " + e.getMessage(), Toast.LENGTH_LONG).show();
			finish();
		}

		// set the title based on the template name
		this.setTitle(Survey.getTemplate().name());

		// show the darn thing already...
		setContentView(R.layout.activity_question_presenter);
	}

	@Override
	public void onPause() {
		super.onPause();

		this.finish();
	}

	@Override
	public void onResume() {
		super.onResume();

		try {



			bindHeader();
			bindUi(answerList.getGroup());
		} catch (Exception e) {
			Log.e(TAG, "Error in onResume", e);
			Toast.makeText(this, e.getClass().getSimpleName() + ": " + e.getMessage(), Toast.LENGTH_LONG).show();
			finish();
		}
	}

	private boolean isNew() {
		return (mode.equals(Modes.HEADER) && surveyId < 0)
				|| (mode.equals(Modes.ITEM) && itemId < 0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getMenuInflater().inflate(R.menu.activity_question_presenter, menu);
		return true;
	}

	private void bindHeader() {

		try {

			Cultures language = Survey.currentCulture();
			TextView title = (TextView) findViewById(R.id.titleLabel);

			if (isNew()) {
				title.setText(String.format("%s %s",
						getResources().getString(R.string.survey_new),
						answerList.getCaption(language)));
			} else {
				if (mode.equalsIgnoreCase(Modes.HEADER)) {
					SurveySummary summary;

					summary = Surveys.getSavedSurvey(surveyId);
					title.setText(String.format("%s - %d",
							answerList.getCaption(language),
							summary.getCaseNumber()));
				} else {
					ItemSummary summary = Surveys.getSavedSurveyItem(surveyId,
							itemId);
					
					title.setText(String.format("%s - %d",
							answerList.getCaption(language), summary.getCaseNumber()));
				}

			}

		} catch (Exception e) {
			Log.e(TAG, "Error in onResume", e);
			Toast.makeText(this,  e.getClass().getSimpleName() + ": " + e.getMessage(), Toast.LENGTH_LONG).show();
			finish();
		}

	}

	private void bindUi(QuestionGroup group) {

		// clear the control map ( we are startig over)
		// controlMap.clear();
		
		Cultures culture = Survey.currentCulture();

		TextView subTitle = (TextView) findViewById(R.id.subtitleLabel);
		subTitle.setText(group.getCaption(culture));

		LinearLayout placeholder = (LinearLayout) findViewById(R.id.contentPlaceholder);

		// clear the canvas
		placeholder.removeAllViews();
		((ScrollView)placeholder.getParent().getParent()).scrollTo(0, 0);
		
		
		// Disable the previous button if there are no previous questions
		View prevButton = findViewById(R.id.previousButton);
		if(answerList.atStart()) {
		    prevButton.setVisibility(View.INVISIBLE);
		} else {
		    prevButton.setVisibility(View.VISIBLE);
		}
		

		for (QuestionBase question : group.getQuestions()) {
			if (question == null)
				break;

			// create and display the label (AFAIK there is no way to set the
			// style in code
			// otherwise, this needs to be cleaned up.
			TextView label = new TextView(getBaseContext());

			if (question.isCritical())
				label.setText(question.getCaption(culture) + " *");
			else
				label.setText(question.getCaption(culture));

			label.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
			label.setTextColor(Color.BLACK);
			label.setPadding(5, 20, 5, 5);

			if (!question.isOptional())
				label.setTypeface(null, Typeface.BOLD);

			placeholder.addView(label);

			// create editors (as many as the question allows)
			for (int i = 0; i < question.maxAnswers(); i++) {
				try {
					Answer answer = answerList.getAnswer(question);
					String value = answer.getValue(i);

					View editor = makeEditor(question);

					editor.setTag(R.id.TAG_ANSWER, answer);
					editor.setTag(R.id.TAG_INDEX, i);
					editor.setTag(R.id.TAG_LABEL, label);

					if (question instanceof TextSelectQuestion) {
						value = ((TextSelectQuestion) question).getOption(
								Survey.currentCulture(), value);
					}

					setValue(editor, value);

					if (question.isPrimaryKey() && answer.getValue(i) != null
							&& answer.getValue(i).length() > 0)
						editor.setEnabled(false);

					/*
					 * Log.d(TAG, "before setValue.. " + answer.getValue(i));
					 * if(answer.getValue()!=null) { setValue(editor,
					 * answer.getValue()); }
					 */

					placeholder.addView(editor);
				} catch (Exception e) {
					Log.getStackTraceString(e);

					Toast.makeText(
							this,
							"An error has occured in the application: \r\n"
									+ e.getClass().getSimpleName() + ": " + e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}
		}
	}

	/**
	 * Creates an instance of an editor based on the question type.
	 * 
	 * @param question
	 * @return EidtText, Checkbox or a Spinner
	 */
	private View makeEditor(QuestionBase question) {

		if (question instanceof TextSelectQuestion) {
			// this question type requires a spinned that includes all provided
			// options

			Spinner spinner = new FocusableSpinner(this);
		
			Cultures culture = Survey.currentCulture();

			String[] strings = ((TextSelectQuestion) question)
					.getOptions(culture);

			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, strings);
			adapter.setDropDownViewResource(R.layout.wrapping_spinner);

			spinner.setAdapter(adapter);

			return spinner;

		} else if (question instanceof TextQuestion) {

			// this requires a simple vanilla text box
			EditText editor = new EditText(getBaseContext());

			editor.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);

			return editor;

		} else if (question instanceof NumericQuestion) {

			// this is a text box set to numeric mode
			EditText editor = new EditText(getBaseContext());

			if (((NumericQuestion) question).getAllowDecimal()) {
				editor.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
			} else {
				editor.setInputType(InputType.TYPE_CLASS_NUMBER);
			}

			return editor;

		} else if (question instanceof BooleanQuestion) {
			// checkbox is fine for boolean. Might need to change to two radio
			// buttons.
			return new CheckBox(getBaseContext());
		}

		// if we made it here, we must've encountered an unexpected question
		// type
		return null;
	}

	public void moveNext(View view) {


		if (commitAnswers(true))
			doMoveNext();
	}

	public void doMoveNext() {
		// this.answerList = Surveys.getHeaderAnswers(surveyId);
	    
		QuestionGroup group = jumpTo != null ? answerList.jump(jumpTo)
				: answerList.moveNext();

		if (group != null) {

			bindUi(group);
		} else {

			if (mode == Modes.HEADER) {
				Surveys.saveValidFlag(surveyId, true);
				Surveys.saveCompletedFlag(surveyId, true);

				Intent intent = new Intent(QuestionPresenter.this, Items.class);
				Bundle b = new Bundle();
				b.putLong(BundleKeys.SURVEY_ID, surveyId);
				intent.putExtras(b);
				startActivity(intent);
				finish();
			} else {
				finish();
			}
		}
	}

	public void movePrevious(View view) {

		// before going anywhere, save the answers we have so far

		commitAnswers(false);

		QuestionGroup group = answerList.movePrevious();

		if (group != null) {
			bindUi(group);
		}
	}

	public boolean commitAnswers(boolean validate) {

		// Set<View> views = controlMap.keySet();

		ArrayList<String> errors = new ArrayList<String>();
		boolean hasCriticalErrors = false;
		jumpTo = null;
		boolean isValid = true;

		LinearLayout placeholder = (LinearLayout) findViewById(R.id.contentPlaceholder);

		int childCount = placeholder.getChildCount();

		for (int viewIndex = 0; viewIndex < childCount; viewIndex++) {
			View view = placeholder.getChildAt(viewIndex);

			if (view.getTag(R.id.TAG_ANSWER) == null)
				continue;

			TextView label = (TextView) view.getTag(R.id.TAG_LABEL);

			Answer answer = (Answer) view.getTag(R.id.TAG_ANSWER);
			QuestionBase question = answer.getQuestion();
			int index = (Integer) view.getTag(R.id.TAG_INDEX);

			String answerValue = getValue(view);

			// special processing for "select" questions

			if (question instanceof TextSelectQuestion) {
				answerValue = Integer.toString(((TextSelectQuestion) question)
						.getOptionId(answerValue, Survey.currentCulture()));
			}

            // If someone hasn't entered a value, we can't accept the empty
			// string even if we aren't validating this.  Empty strings don't
			// parse into numbers, and we don't want them to
			if (validate || answer.getQuestion().isPrimaryKey()) {
				try {
					
					jumpTo = answerList.getGroup().jumpTo();
					
					// if there is more than one, only the last one matters
					// but, if the group has a jumpTo, it takes the priority
					if (jumpTo == null){
					    jumpTo = answer.getQuestion().AcceptAnswer(answerValue);
					}

					label.setTextColor(Color.BLACK);

				} catch (ValidationException e) {

					label.setTextColor(0xFFAA0000);
					errors.add(answer.getQuestion().getCaption(
							Survey.currentCulture()));
					isValid = false;

					// if this is a critical answer, don't allow to move until
					// it's answered.
					if (answer.getQuestion().isCritical())
						hasCriticalErrors = true;

					if (answer.getQuestion().isPrimaryKey()) {

						AlertDialog.Builder builder = new AlertDialog.Builder(
								this);
						builder.setMessage(R.string.survey_caseNumberError)
								.setPositiveButton(R.string.survey_fix, null)
								.show();

						return false; // we can't have any issues with case
										// numbers...
					}
				}
			}

			answer.putValue(index, answerValue);

			if (answer.getQuestion().isPrimaryKey()) {
				// need to create the item.
			    
			    int caseNumber = 0;
			    try {
			        caseNumber = Integer.parseInt(answerValue);
			    } catch (NumberFormatException e) {
			        return false;
			    }

				if (mode == Modes.HEADER && surveyId < 0) {
					surveyId = Surveys.insertSurvey(
							Survey.getCurrentLocation(), caseNumber,
							Survey.surveyTaker(), Survey.supervisor());
				} else if (mode == Modes.ITEM && itemId < 0) {

					itemId = Surveys.insertSurveyItem(surveyId, caseNumber);

				}

				bindHeader();
			}

			if (mode == Modes.HEADER) {
				Surveys.saveHeaderAnswer(surveyId, answer.getQuestion().id(),
						index, answerValue, answer.getIsSkipped(), isValid);
			} else {
				Surveys.saveItemAnswer(surveyId, itemId, answer.getQuestion()
						.id(), index, answerValue, answer.getIsSkipped(),
						isValid);
			}

			answer.resetDirtyFlag();
		}

		// done answering, see what we need to do next

		if (hasCriticalErrors) {
			Toast.makeText(QuestionPresenter.this, R.string.survey_cantMove,
					Toast.LENGTH_LONG).show();
			return false;
		}

		if (errors.size() > 0) {

			String message = getResources().getString(
					R.string.survey_thereAreErrors);

			for (String s : errors)
				message += ("\r\n\t" + s);

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(message)
					.setPositiveButton(R.string.survey_fix, null)
					.setNegativeButton(R.string.survey_ignore,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									doMoveNext();
								}
							}).show();

			return false;
		}

		return true;
	}

	public String getValue(View view) {

		if (view instanceof EditText) {
			return ((EditText) view).getText().toString();
		} else if (view instanceof Spinner) {
			Object selectedItem = ((Spinner) view).getSelectedItem();
			if (selectedItem != null) {
				return selectedItem.toString();
			} else
				return "";
		} else if (view instanceof CheckBox) {
			return ((CheckBox) view).isChecked() ? "True" : "False";
		}

		return null;
	}

	public void setValue(View view, String value) {

		if (view instanceof EditText) {
			EditText texBox = (EditText) view;
			texBox.setText(value);
		} else if (view instanceof Spinner) {
			Spinner spinner = (Spinner) view;

			@SuppressWarnings("unchecked")
			ArrayAdapter<String> adapter = (ArrayAdapter<String>) spinner
					.getAdapter();

			spinner.setSelection(adapter.getPosition(value));
		} else if (view instanceof CheckBox) {
			CheckBox checkbox = (CheckBox) view;

			checkbox.setChecked(Boolean.parseBoolean(value));
		}
	}
	
}
