package org.africabridge.survey;

import java.io.InputStream;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.africabridge.core.BundleKeys;
import org.africabridge.core.Survey;
import org.africabridge.core.SurveySummary;
import org.africabridge.core.Template;
import org.africabridge.data.OurSQLiteHelper;
import org.africabridge.data.Surveys;
import org.africabridge.exceptions.InvalidStateException;
import org.africabridge.world.District;
import org.w3c.dom.Document;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Home extends FragmentActivity {

	private Surveys datasource;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// create a reference to preferences on the Survey
		Survey.setPreferences(PreferenceManager
				.getDefaultSharedPreferences(this));

		datasource = new Surveys(this);
		datasource.open();

		new InitializeTemplateTask().execute();

		OptionMenuFragment.attachOptionMenu(getSupportFragmentManager());

		// Set the version number to display on the home screen
		String versionString = "v";
		PackageInfo packInfo;
		try {
		    packInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
		    versionString = "v " + packInfo.versionName;
		} catch (NameNotFoundException e) {
		    versionString += "?";
		}

		// initialize the View
		setContentView(R.layout.activity_home);

	    TextView versionView = (TextView) findViewById(R.id.homeVersionNum);
	    versionView.setText(versionString);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		/*
		 * MenuInflater inflater = getMenuInflater();
		 * inflater.inflate(R.menu.activity_home, menu);
		 * inflater.inflate(R.menu.activity_export_village_survey, menu);
		 */
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 * 
	 * @Override public boolean onOptionsItemSelected(MenuItem item) { // switch
	 * (item.getItemId()) { case R.id.menu_Export:
	 * showExportVillageScreen(getCurrentFocus()); return true; default: return
	 * super.onOptionsItemSelected(item); } }
	 */

	// A ProgressDialog object
	private ProgressDialog progressDialog;

	// To use the AsyncTask, it must be subclassed
	private class InitializeTemplateTask extends AsyncTask<Void, Integer, Void> {
		// Before running code in separate thread
		@Override
		protected void onPreExecute() {
			// Create a new progress dialog
			progressDialog = new ProgressDialog(Home.this);
			// Set the progress dialog to display a horizontal progress bar
			progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
			// Set the dialog title to 'Loading...'
			progressDialog.setTitle(getResources().getText(
					R.string.home_loading));
			// Set the dialog message to 'Loading application View, please
			// wait...'
			progressDialog.setMessage(getResources().getText(
					R.string.home_loadingTemplate));
			// This dialog can't be canceled by pressing the back key
			progressDialog.setCancelable(false);
			// This dialog isn't indeterminate
			progressDialog.setIndeterminate(false);
			// The maximum number of items is 100
			progressDialog.setMax(100);
			// Set the current progress to zero
			progressDialog.setProgress(0);
			// Display the progress dialog
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... arg0) {

			try {
				// Get the current thread's token
				synchronized (this) {

					if (Survey.getTemplate() == null) {
						Document doc = null;

						InputStream istr = getApplicationContext()
								.getResources().getAssets()
								.open("templates/mvc.xml");

						DocumentBuilderFactory dbFactory = DocumentBuilderFactory
								.newInstance();
						DocumentBuilder dBuilder = dbFactory
								.newDocumentBuilder();
						doc = dBuilder.parse(istr);

						publishProgress(25);

						if (doc != null) {
							doc.normalizeDocument();
							Template template = new Template(doc);
							Survey.setTemplate(template);
						} else {
							throw new Exception((String) getResources()
									.getText(R.string.home_failedToLoad));
						}

						publishProgress(50);
					}

					if (Survey.district() == null) {
						// load wards and villages

						Document doc = null;

						InputStream istr = getApplicationContext()
								.getResources().getAssets()
								.open("templates/wards.xml");

						DocumentBuilderFactory dbFactory = DocumentBuilderFactory
								.newInstance();
						DocumentBuilder dBuilder = dbFactory
								.newDocumentBuilder();
						doc = dBuilder.parse(istr);

						publishProgress(75);

						District region = new District(doc.getDocumentElement());

						Survey.setRegion(region);
					}

				}
			} catch (Exception e) {
				Log.getStackTraceString(e);
			}
			return null;
		}

		// Update the progress
		@Override
		protected void onProgressUpdate(Integer... values) {
			// set the current progress of the progress dialog
			progressDialog.setProgress(values[0]);
		}

		// after executing the code in the thread
		@Override
		protected void onPostExecute(Void result) {
			// close the progress dialog
			progressDialog.dismiss();
			bindHeader();
			bindList();
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		bindHeader();

		if (Survey.getTemplate() != null) {
			bindList();
		}
	}

	@Override
    protected void onStop() {
        super.onStop();
        progressDialog.dismiss();
    }

    public void bindHeader() {
		org.africabridge.world.Location location = Survey.getCurrentLocation();

		if (location != null && location.getWard() != null
				&& location.getVillage() != null) {
			TextView subTitle = (TextView) this.findViewById(R.id.subTitle);
			subTitle.setText(String.format("%s/%s", location.getWard()
					.getName(), location.getVillage().getName()));
		}
	}

	private void bindList() {
		SurveyAdapter adapter;
		try {
			adapter = new SurveyAdapter(this, 0, Surveys.getSavedSurveys(Survey.ward(), Survey.village(), Survey.hideExported()));
		} catch (InvalidStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(Home.this,
					getResources().getString(R.string.item_error),
					Toast.LENGTH_LONG).show();
			return;
		}
		ListView listview = (ListView) findViewById(R.id.surveyList);
		listview.setAdapter(adapter);

		listview.setOnItemClickListener(new ListView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> a, View v, int i, long l) {

				if (!canStartHousehold())
					return;

				// get the underlying item
				SurveySummary item = (SurveySummary) a.getItemAtPosition(i);

				if (item.getIsValid()) 
				{
					// resume survey (survey is valid, so skip to questions)
					
					Intent intent = new Intent(Home.this, Items.class);
					Bundle b = new Bundle();
					
					b.putLong(BundleKeys.SURVEY_ID, item.getId()); // Your id

					intent.putExtras(b); // Put your id to your next Intent
					startActivity(intent);
				} 
				else {
					// resume survey
					Intent intent = new Intent(Home.this, QuestionPresenter.class);
					Bundle b = new Bundle();

					b.putLong(BundleKeys.SURVEY_ID, item.getId()); // Your id
					b.putString(BundleKeys.MODE, QuestionPresenter.Modes.HEADER);
					intent.putExtras(b); // Put your id to your next Intent
					startActivity(intent);
				}
			}
		});
	}

	/* Event handlers */

	public void createNewHousehold(View view) {

		if (!canStartHousehold())
			return;

		Intent intent = new Intent(Home.this, QuestionPresenter.class);
		Bundle b = new Bundle();
		b.putString(BundleKeys.MODE, QuestionPresenter.Modes.HEADER);
		intent.putExtras(b); // Put your id to your next Intent
		startActivity(intent);
	}

	public void showLocationScreen(View view) {
		Intent myIntent = new Intent(Home.this, Location.class);
		startActivity(myIntent);
	}

	private class SurveyAdapter extends ArrayAdapter<SurveySummary> {

		public SurveyAdapter(Context context, int textViewResourceId,
				List<SurveySummary> objects) {
			super(context, textViewResourceId, objects);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			// only inflate the view if it's null
			if (view == null) {
				LayoutInflater vi = (LayoutInflater) this.getContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = vi.inflate(R.layout.layout_survey_summary, null);
			}

			SurveySummary item = (SurveySummary) this.getItem(position);

			TextView title = (TextView) view.findViewById(R.id.title);
			title.setText(String.format(
					"%s %s",
					Survey.getTemplate().headerQuestions()
							.getCaption(Survey.currentCulture()), item.getFullNumber()));

			TextView subTitle = (TextView) view.findViewById(R.id.subTitle);
			subTitle.setText(String.format("%s %s", item.getSurveyTaker(), ""));

			TextView detail = (TextView) view.findViewById(R.id.detail);
			detail.setText(String.format(
					getResources().getString(R.string.item_createdOn),
					item.getCreatedOn()));

			if (!item.getIsValid()) {
				title.setTextColor(Color.RED);
				subTitle.setTextColor(Color.RED);
				detail.setTextColor(Color.RED);
			} else {
				title.setTextColor(Color.BLACK);
				subTitle.setTextColor(Color.BLACK);
				detail.setTextColor(Color.BLACK);
			}

			return (view);
		}
	}

	private boolean canStartHousehold() {
		if (Survey.getCurrentLocation() == null
				|| !Survey.getCurrentLocation().isValid()) {
			Toast.makeText(Home.this, R.string.home_locationNotSet,
					Toast.LENGTH_LONG).show();

			Intent intent = new Intent(Home.this, Location.class);
			Home.this.startActivity(intent);
			return false;
		}

		if (!Survey.areSettingValid()) {
			Toast.makeText(Home.this, R.string.home_settingsNotSet,
					Toast.LENGTH_LONG).show();

			Intent intent = new Intent(Home.this, SettingsActivity.class);
			Home.this.startActivity(intent);
			return false;
		}

		return true;
	}

	/* OLD CODE BELOW. */

	public void createNewChild(View view) {
		Intent intent = new Intent(Home.this, QuestionPresenter.class);

		Bundle b = new Bundle();
		b.putString("mode", "item"); // Your id
		intent.putExtras(b); // Put your id to your next Intent

		startActivity(intent);
	}

	public void showSettingsScreen(View view) {
		// Is Home.this really necessary? Got this from a tutorial, not sure...
		Intent myIntent = new Intent(Home.this, SettingsActivity.class);
		Home.this.startActivity(myIntent);
	}

	public void showHouseholdScreen(View view) {
		// Intent myIntent = new Intent(Home.this, Households.class);
		// Home.this.startActivity(myIntent);
	}

}
