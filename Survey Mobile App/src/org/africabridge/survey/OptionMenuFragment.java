package org.africabridge.survey;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class OptionMenuFragment extends Fragment {

	private static final String TAG = "OptionsMenu";

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return null;
	}

	@Override
	public void onPause() {
		super.onPause();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	    inflater.inflate(R.menu.options_menu, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent myIntent;
		switch (item.getItemId()) {
		case R.id.menu_export:
			myIntent = new Intent(getActivity(), Export.class);

			OptionMenuFragment.this.startActivity(myIntent);
			return true;
		case R.id.menu_settings:

			myIntent = new Intent(getActivity(), SettingsActivity.class);
			OptionMenuFragment.this.startActivity(myIntent);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
	}
	
	public static void attachOptionMenu(FragmentManager manager) {
		Fragment frag = manager.findFragmentByTag("setting-option-menu");
		
		if(frag == null) {
			FragmentTransaction fragmentTransaction = manager.beginTransaction();       
        	OptionMenuFragment optionMenuFragment = new OptionMenuFragment();
        	fragmentTransaction.add(optionMenuFragment, "setting-option-menu");
        	fragmentTransaction.commit();
		}
	}
}
