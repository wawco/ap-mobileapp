package org.africabridge.survey;

import java.util.Locale;

import org.africabridge.core.Cultures;
import org.africabridge.core.Survey;
import org.africabridge.core.SurveyApplication;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

//import android.support.v4.app.NavUtils;

public class SettingsActivity extends FragmentActivity {
	private static final String TAG = "SettingsActivity";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		OptionMenuFragment.attachOptionMenu(getSupportFragmentManager());

		setContentView(R.layout.activity_settings);

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, Cultures.values());
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		((Spinner) this.findViewById(R.id.languageSpinner)).setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getMenuInflater().inflate(R.menu.activity_settings, menu);
		return true;
	}

	public void saveAndClose(View view) {

		String surveyTaker = ((EditText) this
				.findViewById(R.id.surveyTakerEditor)).getText().toString();
		String supervisor = ((EditText) this
				.findViewById(R.id.supervisorEditor)).getText().toString();
		String language = (String) ((Spinner) this
				.findViewById(R.id.languageSpinner)).getSelectedItem();

		boolean hideExported = ((CheckBox) this
				.findViewById(R.id.hideExportedCheckBox)).isChecked();

		// validation code

		boolean hasErrors = false;

		if (surveyTaker == null || surveyTaker.trim().length() == 0) {
			setInvalidLabel(R.id.surveyTakerLabel);
			hasErrors = true;
		} else {
			setValidLabel(R.id.surveyTakerLabel);
		}

		if (supervisor == null || supervisor.trim().length() == 0) {
			setInvalidLabel(R.id.supervisorLabel);
			hasErrors = true;
		} else {
			setValidLabel(R.id.supervisorLabel);
		}

		if (language == null || language.trim().length() == 0) {
			setInvalidLabel(R.id.languageLabel);
			hasErrors = true;
		} else {
			setValidLabel(R.id.languageLabel);
		}

		if (!hasErrors) {
			Survey.setPreferences(language, surveyTaker, supervisor,
					hideExported);

			/*
			 * The next bit is a little weird.  It looks like a standard clearing of the activity stack,
			 * and re-launching the main activity.  However, we need to essentially "restart" the application.
			 * See: http://stackoverflow.com/questions/6609414/howto-programatically-restart-android-app/13173279#13173279
			 *
			 * System.exit(1) forces ActivityManager to rebuild everything similar to what it does
			 * if configuration change event was triggered.
			 *
			 * A preferable way would just to be to trigger a configuration change, since we can handle those at the App
			 * level now.
			 */
			Intent i = getBaseContext().getPackageManager()
					.getLaunchIntentForPackage(
							getBaseContext().getPackageName());
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			System.exit(1);
		} else {
			showErrorDialog();
		}
	}

	private void setInvalidLabel(int labelId) {
		TextView label = (TextView) this.findViewById(labelId);

		label.setTextColor(0xFFAA0000);
	}

	private void setValidLabel(int labelId) {
		TextView label = (TextView) this.findViewById(labelId);

		label.setTextColor(Color.BLACK);
	}

	private void showErrorDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(R.string.settings_error)
				.setPositiveButton(R.string.settings_fix, null).show();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		// restore values from the settings

		((EditText) this.findViewById(R.id.surveyTakerEditor)).setText(Survey
				.surveyTaker());
		((EditText) this.findViewById(R.id.supervisorEditor)).setText(Survey
				.supervisor());
		((CheckBox) this.findViewById(R.id.hideExportedCheckBox))
				.setChecked(Survey.hideExported());

		Spinner spinner = ((Spinner) this.findViewById(R.id.languageSpinner));

		@SuppressWarnings("unchecked")
		ArrayAdapter<String> adapter = (ArrayAdapter<String>) spinner
				.getAdapter();

		spinner.setSelection(adapter.getPosition(Survey.currentCulture()
				.name()));
	}
}
