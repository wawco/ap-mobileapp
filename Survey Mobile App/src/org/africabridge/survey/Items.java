package org.africabridge.survey;

import java.util.List;
import java.util.Locale;

import org.africabridge.core.BundleKeys;
import org.africabridge.core.ItemSummary;
import org.africabridge.core.Survey;
import org.africabridge.core.SurveySummary;
import org.africabridge.data.Surveys;
import org.africabridge.exceptions.InvalidStateException;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

//NOTE: To get the instance of the survey call Survey.getInstance();

public class Items extends FragmentActivity {

	private static final String TAG = "ItemsActivity";

	private SurveySummary summary;
	private long surveyId;

	private SurveySummary getSummary() throws InvalidStateException {
		if (summary == null || summary.getId() != surveyId)
			summary = Surveys.getSavedSurvey(surveyId);

		return summary;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		// Option menu fragment stuff. @todo: lots of duplicate code across
		// classes, need to fix
		OptionMenuFragment.attachOptionMenu(getSupportFragmentManager());

		// ID's are passed via the bundle
		Bundle b = getIntent().getExtras();
		
		if (b == null || !b.containsKey(BundleKeys.SURVEY_ID)) {
			// this is bad: we should always have an ID
			Toast.makeText(this,
					getResources().getString(R.string.items_noSurveyId),
					Toast.LENGTH_LONG).show();
			finish();
		} else {

			this.surveyId = b.getLong(BundleKeys.SURVEY_ID);
		}

		this.setTitle(Survey.getTemplate().name());
		setContentView(R.layout.activity_items);
	}

	@Override
	public void onResume() {
		super.onResume();

		// bind the list view
		ItemAdapter adapter = new ItemAdapter(this, 0,
				Surveys.getSavedSurveyItems(surveyId));
		ListView listview = (ListView) findViewById(R.id.surveyList);
		listview.setAdapter(adapter);
		
		
		TextView subTitle = (TextView) this.findViewById(R.id.subTitle);
		
		try {
			subTitle.setText(String.format("%s %02d-%02d-%03d", 
					Survey.getTemplate().headerQuestions().getCaption(Survey.currentCulture()),
					getSummary().getLocation().getWard().getId(),
					getSummary().getLocation().getVillage().getId(),
					getSummary().getCaseNumber()));
		} catch (InvalidStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(Items.this,
					getResources().getString(R.string.item_error),
					Toast.LENGTH_LONG).show();
			return;
		}

		listview.setOnItemClickListener(new ListView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> a, View v, int i, long l) {
				// get the underlying item
				ItemSummary item = (ItemSummary) a.getItemAtPosition(i);

				// resume survey
				Intent intent = new Intent(Items.this, QuestionPresenter.class);

				Bundle b = new Bundle();
				b.putString(BundleKeys.MODE, QuestionPresenter.Modes.ITEM);
				b.putLong(BundleKeys.SURVEY_ID, item.getSurveyId());
				b.putLong(BundleKeys.ITEM_ID, item.getId()); // TODO: pass the ID of the
													// selected item (for Wes)
				intent.putExtras(b);

				startActivity(intent);
			}
		});
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getMenuInflater().inflate(R.menu.activity_items, menu);
		return true;
	}

	public void createNewChild(View view) {
		Intent intent = new Intent(Items.this, QuestionPresenter.class);

		Bundle b = new Bundle();
		b.putString(BundleKeys.MODE, QuestionPresenter.Modes.ITEM);
		b.putLong(BundleKeys.SURVEY_ID, surveyId);
		intent.putExtras(b);

		startActivity(intent);
	}
	
	public void returnHome(View view) {
//		Intent intent = new Intent(Items.this, Home.class);
//		
//		Bundle b = new Bundle();
//		intent.putExtras(b);
//		
//		
//		startActivity(intent);
		
		/*
		 * I changed to this - because we don't want to be able to
		 * press the back button and go back to the children screen.
		 * So, rather than starting a new activity we will just finish
		 * up this one.
		 */
		this.finishActivity(RESULT_OK);
		this.finish();
	}

	public void resumeChild(View view) {
		Intent intent = new Intent(Items.this, QuestionPresenter.class);

		Bundle b = new Bundle();
		b.putLong("id", surveyId);
		b.putLong("itemId", 0); // TODO: pass the ID of the selected item (for
								// Wes)
		intent.putExtras(b);

		startActivity(intent);
	}

	private class ItemAdapter extends ArrayAdapter<ItemSummary> {

		public ItemAdapter(Context context, int textViewResourceId,
				List<ItemSummary> objects) {
			super(context, textViewResourceId, objects);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			// only inflate the view if it's null
			if (view == null) {
				LayoutInflater vi = (LayoutInflater) this.getContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = vi.inflate(R.layout.layout_survey_summary, null);
			}

			ItemSummary item = (ItemSummary) this.getItem(position);
try {
			// get text view
			
				TextView label = (TextView) view.findViewById(R.id.title);
				
				label.setText(String.format("%02d-%02d-%03d-%02d",
					getSummary().getLocation().getWard().getId(),
					getSummary().getLocation().getVillage().getId(), 
					getSummary().getCaseNumber(),
					item.getCaseNumber()));

				TextView subTitle = (TextView) view.findViewById(R.id.subTitle);
			
				subTitle.setText("");
				
				TextView detail = (TextView) view.findViewById(R.id.detail);
				detail.setText(String.format(
						getResources().getString(R.string.item_createdOn),
						item.getCreatedOn()));
				
				
			} catch (InvalidStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Toast.makeText(Items.this,
						getResources().getString(R.string.item_error),
						Toast.LENGTH_LONG).show();
				return null;
			}

			return (view);
		}

	}

	public void editHeader(View view) {
		// resume survey
		Intent intent = new Intent(Items.this, QuestionPresenter.class);
		Bundle b = new Bundle();
		b.putString(BundleKeys.MODE, QuestionPresenter.Modes.HEADER);
		b.putLong(BundleKeys.SURVEY_ID, surveyId); // Your id
		intent.putExtras(b); // Put your id to your next Intent
		startActivity(intent);
	}

}
