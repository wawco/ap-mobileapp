package org.africabridge.survey;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.africabridge.core.*;
import org.africabridge.data.Surveys;
import org.africabridge.exceptions.InvalidStateException;
import org.africabridge.questions.QuestionBase;
import org.africabridge.questions.TextSelectQuestion;
import org.xmlpull.v1.XmlSerializer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Export extends FragmentActivity {

	private static String TAG = "Export";
	private static String[] header = { "S/N", "AB Number", "Region", "Council",
			"Ward", "Village", "Household Number", "First Name", "Last Name",
			"Gender of Parent/Guardian", "Age of Parent/Guardian",
			"Employment of Parent/Guardian", "Number of male dependents",
			"Number of female dependents", "Case number", "First Name",
			"Last Name", "Gender of Child", "Year of Birth of the Child",
			"Month of Birth of the Child", "Day of Birth of the Child",
			"Parent/guardian's relationship to the Child",
			"Child Group for the Child", "Is the child a Vulnerable Child?",
			"Reasons for Vulnerability for the Child",
			"Date of Original Identification for the Child",
			"Date of Exit for the Child", "Reason for Exit for the Child",
			"Nutritional Status for the Child",
			"Vaccinations Complete for the Child", "Is the Child in School?",
			"Reason for Being Out of School",
			"Educational Level at Drop-Out for the the Child",
			"Current Educational Level for the Child",
			"Priority Needs for the Child", "Services Received for the Child",
			"Service Provider for the Child" };
	private static final String CREATED_ON_ATTRIBUTE = "created-on";
	private static final String GUID_ATTRIBUTE = "guid";
	private static final String DEVICE_ID_ATTRIBUTE = "device-id";
	private static final String ID_ATTRIBUTE = "id";
	private static final String VALUE_TAG = "value";
	private static final String LABEL_ATTRIBUTE = "label";
	private static final String CODE_ATTRIBUTE = "code";
	private static final String PROPERTY_ATTRIBUTE = "property";
	private static final String QUESTION_TAG = "question";
	private static final String LONGITUDE_TAG = "longitude";
	private static final String LATITUDE_TAG = "latitude";
	private static final String COORDINATE_TAG = "coordinate";
	private static final String SUB_VILLAGE_TAG = "sub-village";
	private static final String VILLAGE_TAG = "village";
	private static final String WARD_TAG = "ward";
	private static final String DISTRICT_TAG = "district";
	private static final String INTERVIEWER_TAG = "interviewer";
	private static final String NAME_TAG = "name";
	private static final String SUPERVISOR_TAG = "supervisor";
	private static final String LOCATION_TAG = "location";
	private static final String QUESTIONS_TAG = "questions";
	private static final String SN_TAG = "sn";
	private static final String ITEM_TAG = "item";
	private static final String ITEMS_TAG = "items";
	private static final String SURVEY_TAG = "survey";

	private boolean externalStorageWriteable = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		OptionMenuFragment.attachOptionMenu(getSupportFragmentManager());

		setContentView(R.layout.activity_export);

		CheckBox skipCheckbox = (CheckBox) findViewById(R.id.skipIncompCheckBox);

		skipCheckbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				bindSpinner();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getMenuInflater().inflate(R.menu.activity_export_village_survey,
		// menu);
		return true;
	}

	@Override
	public void onResume() {

		super.onResume();

		bindSpinner();

		org.africabridge.world.Location location = Survey.getCurrentLocation();

		if (location != null && location.getWard() != null
				&& location.getVillage() != null) {
			TextView subTitle = (TextView) this.findViewById(R.id.subTitle);
			subTitle.setText(String.format("%s/%s", location.getWard()
					.getName(), location.getVillage().getName()));
		}
	}

	public void bindSpinner() {
		CheckBox skipCheckbox = (CheckBox) findViewById(R.id.skipIncompCheckBox);

		// bind the list view
		// TODO: Add location once there is a way to get current location
		ExportAdapter adapter;
		try {
			adapter = new ExportAdapter(this, 0, Surveys.getDirtySurveys(
					Survey.ward(), Survey.village(),
					skipCheckbox.isChecked() == true, Survey.hideExported()));
		} catch (InvalidStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(Export.this,
					getResources().getString(R.string.item_error),
					Toast.LENGTH_LONG).show();
			return;
		}
		ListView listview = (ListView) findViewById(R.id.surveyList);
		listview.setAdapter(adapter);
	}

	public void showLocationScreen(View view) {
		Intent myIntent = new Intent(Export.this, Location.class);
		Export.this.startActivity(myIntent);
	}

	public void browse(View view) {
		// TODO: Dung - open "select folder" dialog window here
	}

	/**
	 * Check for external storage status if the android device has any external
	 * storage which does not need to be a SD card
	 * 
	 * @param state
	 *            from Environment.getExternalStorageState() call
	 */
	public void checkExternalStorageStat(String state) {
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			externalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
			externalStorageWriteable = false;
		} else {
			externalStorageWriteable = false;
		}
	}

	public class prefKeys {
		private static final String DEVICE_ID = "deviceID";
	}

	private class ExportAdapter extends ArrayAdapter<SurveySummary> {

		public ExportAdapter(Context context, int textViewResourceId,
				List<SurveySummary> objects) {
			super(context, textViewResourceId, objects);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			// only inflate the view if it's null
			if (view == null) {
				LayoutInflater vi = (LayoutInflater) this.getContext()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = vi.inflate(R.layout.layout_survey_summary, null);
			}

			SurveySummary item = (SurveySummary) this.getItem(position);

			TextView title = (TextView) view.findViewById(R.id.title);
			title.setText(String.format("%s %s", Survey.getTemplate()
					.headerQuestions().getCaption(Survey.currentCulture()),
					item.getFullNumber()));

			TextView subTitle = (TextView) view.findViewById(R.id.subTitle);
			subTitle.setText(String.format("%s %s", item.getSurveyTaker(), ""));

			TextView detail = (TextView) view.findViewById(R.id.detail);
			detail.setText(String.format(
					getResources().getString(R.string.item_createdOn),
					item.getCreatedOn()));

			if (!item.getIsValid()) {
				title.setTextColor(Color.GRAY);
				subTitle.setTextColor(Color.GRAY);
				detail.setTextColor(Color.GRAY);
			} else {
				title.setTextColor(Color.BLACK);
				subTitle.setTextColor(Color.BLACK);
				detail.setTextColor(Color.BLACK);
			}

			return (view);
		}
	}

	// A ProgressDialog object
	private ProgressDialog progressDialog;

	public void exportSurveys(View view) {

		// To use the AsyncTask, it must be subclassed
		class ExportToXmlTask extends AsyncTask<Void, Integer, Void> {

			private Throwable error = null;

			// Before running code in separate thread
			@Override
			protected void onPreExecute() {
				// Create a new progress dialog
				progressDialog = new ProgressDialog(Export.this);
				// Set the progress dialog to display a horizontal progress bar
				progressDialog
						.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
				// Set the dialog title to 'Loading...'
				progressDialog.setTitle(getResources().getString(
						R.string.export_exporting));
				// Set the dialog message to 'Loading application View, please
				// wait...'
				progressDialog.setMessage(getResources().getString(
						R.string.export_exportingMessage));
				// This dialog can't be canceled by pressing the back key
				progressDialog.setCancelable(false);
				// This dialog isn't indeterminate
				progressDialog.setIndeterminate(false);
				// The maximum number of items is 100
				progressDialog.setMax(100);
				// Set the current progress to zero
				progressDialog.setProgress(0);
				// Display the progress dialog
				progressDialog.show();
			}

			@Override
			protected Void doInBackground(Void... arg0) {

				try {
					// Get the current thread's token
					synchronized (this) {

						// the functionality to write the XML to the selected
						// folder
						// TODO check device status

						File storageDir;
						String deviceSDcardStatus = Environment
								.getExternalStorageState();
						checkExternalStorageStat(deviceSDcardStatus);
						// a SD card exists
						if (externalStorageWriteable) {
							// the storage directory is "/mnt/sdcard" in this
							// case
							storageDir = Environment
									.getExternalStorageDirectory();
						} else {
							/**
							 * If there is no SD card then the app will write
							 * .xml files to internal storage.The storage
							 * directory is:
							 * "/data/data/org.africabridge.survey/app_mvc/mvc"
							 */
							storageDir = getDir("mvc",
									Context.MODE_WORLD_WRITEABLE);
						}

						Log.d("nicole", "trying to export file to "
								+ storageDir.toString());
						File mvcFile = new File(storageDir, "/mvc");
						if (!(mvcFile.exists())) {
							mvcFile.mkdir();
						}

						exportSurveys(mvcFile);

						// this be under construction
						// exportSurveysExcel(mvcFile);

						publishProgress(100);
					}
				} catch (Exception e) {
					Log.e(TAG, Log.getStackTraceString(e));
					error = e;
					return null;
				}
				return null;
			}

			private void exportSurveys(File mvcFile)
					throws InvalidStateException, IOException,
					FileNotFoundException {
				int progress = 0;

				XmlSerializer serializer = Xml.newSerializer();

				CheckBox skipCheckbox = (CheckBox) findViewById(R.id.skipIncompCheckBox);
				List<SurveySummary> surveys = Surveys
						.getDirtySurveys(Survey.ward(), Survey.village(),
								skipCheckbox.isChecked() == true,
								Survey.hideExported());

				SimpleDateFormat fileNameDateFormatter = new SimpleDateFormat(
						"yyyyMMddHHmmsss");

				// make sure there are actually existing surveys to export by
				// validating the size
				if (surveys.size() > 0) {
					int increment = 100 / surveys.size();

					for (SurveySummary survey : surveys) {
						publishProgress(progress += increment);

						// you are saving each household to a new file.

						String fileName = String.format("%s-%s.xml", survey
								.getFullNumber(), fileNameDateFormatter
								.format(new java.util.Date().getTime()));

						File xmlFile = new File(mvcFile, fileName);

						xmlFile.createNewFile();

						SimpleDateFormat formater = new SimpleDateFormat(
								"yyyy-MM-dd HH:mm:sss");

						FileOutputStream fileWriter = new FileOutputStream(
								xmlFile);
						OutputStreamWriter writer = new OutputStreamWriter(
								fileWriter);

						// make up UUID w/o saving to prefs -->TODO save to
						// prefs
						String device_id = Survey.installationId();
						String GUID = UUID.randomUUID().toString();

						serializer.setOutput(writer);
						serializer.startDocument("UTF-8", true);
						serializer
								.setFeature(
										"http://xmlpull.org/v1/doc/features.html#indent-output",
										true);
						serializer
								.processingInstruction("xml-stylesheet type=\"text/xsl\" href=\"style.xslt\"");
						serializer.startTag("", SURVEY_TAG);
						serializer.attribute("", SN_TAG,
								String.valueOf(survey.getId()));
						serializer.attribute("", DEVICE_ID_ATTRIBUTE,
								device_id.toUpperCase());
						serializer.attribute("", GUID_ATTRIBUTE,
								GUID.toUpperCase());
						serializer.attribute("", CREATED_ON_ATTRIBUTE,
								formater.format(survey.getCreatedOn()));
						serializer.attribute("", "xmlns:xsi",
								"http://www.w3.org/2001/XMLSchema-instance");
						serializer.attribute("",
								"xsi:noNamespaceSchemaLocation", "output.xsd");

						serializer.startTag("", INTERVIEWER_TAG);
						serializer.startTag("", NAME_TAG);
						serializer.text(survey.getSurveyTaker());
						serializer.endTag("", NAME_TAG);
						serializer.endTag("", INTERVIEWER_TAG);

						serializer.startTag("", SUPERVISOR_TAG);
						serializer.startTag("", NAME_TAG);
						serializer.text(survey.getSupervisor());
						serializer.endTag("", NAME_TAG);
						serializer.endTag("", SUPERVISOR_TAG);

						serializer.startTag("", LOCATION_TAG);
						// check in case location is null
						if (survey.getLocation() != null) {

							writeLocation(serializer, survey);

						}
						serializer.endTag("", LOCATION_TAG);

						Answer[] headAnswers = Surveys.getHeaderAnswers(
								survey.getId()).getAllAnswers();

						serializer.startTag("", QUESTIONS_TAG);

						for (Answer answer : headAnswers) {
							writeQuestion(serializer, answer);
						}

						serializer.endTag("", QUESTIONS_TAG);

						List<ItemSummary> items = Surveys
								.getSavedSurveyItems(survey.getId());

						serializer.startTag("", ITEMS_TAG);

						for (ItemSummary item : items) {
							Answer[] answers = Surveys.getItemAnswers(
									survey.getId(), item.getId())
									.getAllAnswers();

							serializer.startTag("", ITEM_TAG);
							serializer.attribute("", SN_TAG,
									String.valueOf(item.getId()));

							for (Answer answer : answers) {
								writeQuestion(serializer, answer);
							}

							serializer.endTag("", ITEM_TAG);
						}
						serializer.endTag("", ITEMS_TAG);

						serializer.endTag("", SURVEY_TAG);
						serializer.endDocument();
						serializer.flush();
						writer.flush();
						writer.close();

						Surveys.saveExportDate(survey.getId(),
								new java.util.Date());
					}
				}

			}

			private void exportSurveysExcel(File mvcFile)
					throws InvalidStateException, IOException,
					FileNotFoundException {
				int progress = 0;
				SimpleDateFormat formater = new SimpleDateFormat(
						"yyyy-MM-dd HH:mm:sss");
				FileOutputStream fileWriter;
				OutputStreamWriter writer;
				// XmlSerializer serializer = Xml.newSerializer();

				CheckBox skipCheckbox = (CheckBox) findViewById(R.id.skipIncompCheckBox);
				List<SurveySummary> surveys = Surveys
						.getDirtySurveys(Survey.ward(), Survey.village(),
								skipCheckbox.isChecked() == true,
								Survey.hideExported());

				SimpleDateFormat fileNameDateFormatter = new SimpleDateFormat(
						"yyyyMMddHHmmsss");

				String fileName = String.format("%s-%s.csv", surveys.get(1)
						.getLocation().getWard().toString(),
						fileNameDateFormatter.format(new java.util.Date()
								.getTime()));

				File csvFile = new File(mvcFile, fileName);

				csvFile.createNewFile();

				fileWriter = new FileOutputStream(csvFile);
				writer = new OutputStreamWriter(fileWriter);

				int len = header.length;

				long surveyid = 0;

				do {

					writer.write(header[len].toString() + ",");
				} while (len > 0);

				int increment = 100 / surveys.size();
				for (SurveySummary survey : surveys) {
					publishProgress(progress += increment);
					surveyid = survey.getId();
					// you are saving each household to a new file.

					// make up UUID w/o saving to prefs -->TODO save to prefs
					String device_id = Survey.installationId();
					String GUID = UUID.randomUUID().toString();

					// serializer.setOutput(writer);
					// serializer.startDocument("UTF-8", true);
					// serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output",
					// true);
					// serializer.processingInstruction("xml-stylesheet type=\"text/xsl\" href=\"style.xslt\"");
					// serializer.startTag("", SURVEY_TAG);
					writer.write(formater.format(survey.getCreatedOn()) + ",");
					// serializer.attribute("", SN_TAG,
					// String.valueOf(survey.getId()));
					writer.write(survey.getId() + ",");
					// serializer.attribute("", DEVICE_ID_ATTRIBUTE,
					// device_id.toUpperCase());
					writer.write(device_id.toUpperCase() + ",");
					// serializer.attribute("", GUID_ATTRIBUTE,
					// GUID.toUpperCase());
					// serializer.attribute("", CREATED_ON_ATTRIBUTE,
					// formater.format(survey.getCreatedOn()));

					// serializer.startTag("", INTERVIEWER_TAG);
					writer.write(INTERVIEWER_TAG + ",");
					// serializer.startTag("", NAME_TAG);
					// serializer.text(survey.getSurveyTaker());
					writer.write(survey.getSurveyTaker().toString() + ",");
					// serializer.endTag("", NAME_TAG);
					// serializer.endTag("", INTERVIEWER_TAG);

					// serializer.startTag("", SUPERVISOR_TAG);
					writer.write(SUPERVISOR_TAG + ",");
					// serializer.startTag("", NAME_TAG);
					// serializer.text(survey.getSupervisor());
					writer.write(survey.getSupervisor().toString() + ",");
					// writer.write(System.getProperty("line.separator"));
					// serializer.endTag("", NAME_TAG);
					// serializer.endTag("", SUPERVISOR_TAG);

					// serializer.startTag("", LOCATION_TAG);
					// check in case location is null

					if (survey.getLocation() != null) {

						writeLocation(writer, survey);

					}
					// serializer.endTag("", LOCATION_TAG);

					Answer[] headAnswers = Surveys.getHeaderAnswers(
							survey.getId()).getAllAnswers();

					// serializer.startTag("", QUESTIONS_TAG);

					int i = 0;
					for (Answer answer : headAnswers) {
						if ((answer.getQuestion().propertyName()) != null
								&& (answer.getQuestion()) != null)
							writer.write(answer.getQuestion().propertyName()
									+ ",");

						// writeQuestion(writer, answer);
					}
					writer.write(System.getProperty("line.separator"));
					// serializer.endTag("", QUESTIONS_TAG);

					// ArrayList<QuestionBase> questions =
					// org.africabridge.core.QuestionList.

					List<ItemSummary> items = Surveys
							.getSavedSurveyItems(survey.getId());

					// serializer.startTag("", ITEMS_TAG);

					for (ItemSummary item : items) {
						Answer[] answers = Surveys.getItemAnswers(
								survey.getId(), item.getId()).getAllAnswers();

						// serializer.startTag("", ITEM_TAG);
						// writer.write(ITEM_TAG + ",")
						// serializer.attribute("", SN_TAG,
						// String.valueOf(item.getId()));

						for (Answer answer : answers) {
							writeQuestion(writer, answer,
									Survey.currentCulture());
							// writer.write(System.getProperty("line.separator"));

						}

						writer.write(System.getProperty("line.separator"));

						// for(Answer answer : answers)
						// {
						// writeAnswer(writer, answer);
						// }

						// serializer.endTag("", ITEM_TAG);
					}
					// serializer.endTag("", ITEMS_TAG);

					// serializer.endTag("", SURVEY_TAG);
					// serializer.endDocument();
					// serializer.flush();

				}
				writer.flush();
				writer.close();

				Surveys.saveExportDate(surveyid, new java.util.Date());

			}

			private void writeLocation(XmlSerializer serializer,
					SurveySummary survey) throws IOException {
				serializer.startTag("", DISTRICT_TAG);
				serializer.attribute(
						"",
						SN_TAG,
						String.valueOf(survey.getLocation().getDistrict()
								.getId()));
				serializer.text(survey.getLocation().getDistrict().getName());
				serializer.endTag("", DISTRICT_TAG);
				serializer.startTag("", WARD_TAG);
				serializer.attribute("", SN_TAG,
						String.valueOf(survey.getLocation().getWard().getId()));
				serializer.text(survey.getLocation().getWard().getName());
				serializer.endTag("", WARD_TAG);
				serializer.startTag("", VILLAGE_TAG);
				serializer.attribute(
						"",
						SN_TAG,
						String.valueOf(survey.getLocation().getVillage()
								.getId()));
				serializer.text(survey.getLocation().getVillage().getName());
				serializer.endTag("", VILLAGE_TAG);
				serializer.startTag("", SUB_VILLAGE_TAG);
				serializer.text(survey.getLocation().getSubVillage());
				serializer.endTag("", SUB_VILLAGE_TAG);
				serializer.startTag("", COORDINATE_TAG);
				serializer.startTag("", LATITUDE_TAG);
				serializer.text(String.valueOf(survey.getLocation()
						.getLatitude()));
				serializer.endTag("", LATITUDE_TAG);
				serializer.startTag("", LONGITUDE_TAG);
				serializer.text(String.valueOf(survey.getLocation()
						.getLongitude()));
				serializer.endTag("", LONGITUDE_TAG);
				serializer.endTag("", COORDINATE_TAG);
			}

			private void writeLocation(OutputStreamWriter writer,
					SurveySummary survey) throws IOException {
				// serializer.startTag("", DISTRICT_TAG);
				// serializer.attribute("", SN_TAG,
				// String.valueOf(survey.getLocation().getDistrict().getId()));
				// serializer.text(survey.getLocation().getDistrict().getName());
				// serializer.endTag("", DISTRICT_TAG);
				// serializer.startTag("", WARD_TAG);
				// serializer.attribute("", SN_TAG,
				// String.valueOf(survey.getLocation().getWard().getId()));
				// serializer.text(survey.getLocation().getWard().getName());
				// serializer.endTag("", WARD_TAG);
				// serializer.startTag("", VILLAGE_TAG);
				// serializer.attribute("", SN_TAG,
				// String.valueOf(survey.getLocation().getVillage().getId()));
				// serializer.text(survey.getLocation().getVillage().getName());
				// serializer.endTag("", VILLAGE_TAG);
				// serializer.startTag("", SUB_VILLAGE_TAG);
				// serializer.text(survey.getLocation().getSubVillage());
				// serializer.endTag("", SUB_VILLAGE_TAG);
				// serializer.startTag("", COORDINATE_TAG);
				// serializer.startTag("", LATITUDE_TAG);
				// serializer.text(String.valueOf(survey.getLocation().getLatitude()));
				// serializer.endTag("", LATITUDE_TAG);
				// serializer.startTag("", LONGITUDE_TAG);
				// serializer.text(String.valueOf(survey.getLocation().getLongitude()));
				// serializer.endTag("", LONGITUDE_TAG);
				// serializer.endTag("", COORDINATE_TAG);
			}

			private void writeQuestion(OutputStreamWriter writer,
					Answer answer, Cultures culture) throws IOException {

				// Log.d(TAG, "Serializing " +
				// answer.getQuestion().propertyName());
				//
				QuestionBase question = answer.getQuestion();
				//
				//
				// serializer.startTag("", QUESTION_TAG);
				//
				// Some questions have no properties
				if (answer.getQuestion().propertyName() != null) {
					// writer.write(System.getProperty("line.separator"));
					writer.write(answer.getQuestion().propertyName() + ",");

					// serializer.attribute("", PROPERTY_ATTRIBUTE,
					// question.propertyName());
				}
				//
				// // Some questions have no code
				// if (question.code() != null) {
				// serializer.attribute("", CODE_ATTRIBUTE,question.code());
				// }

				// writer.write(question.getCaption(org.africabridge.core.Survey.currentCulture())
				// + ",");
				// serializer.attribute("", LABEL_ATTRIBUTE,
				// question.getCaption(Survey.currentCulture()));
				//
				String values[] = answer.getValues();

				// for (int i = 0; i < values.length; i++) {
				//
				// // serializer.startTag("", VALUE_TAG);
				//
				// String value = values[i];
				// //
				// if(value!=null)
				// {
				// if(question instanceof TextSelectQuestion)
				// {
				// String str =
				// ((TextSelectQuestion)question).getOption(Survey.currentCulture(),
				// value);
				//
				// //serializer.attribute("", ID_ATTRIBUTE, value);
				// //serializer.text(str);
				// writer.write(str + ",");
				// }
				// else
				// {
				// writer.write(value + ",");
				// }
				// }
				//
				// }
				//
				//
				// serializer.endTag("", QUESTION_TAG);
			}

			private void writeAnswer(OutputStreamWriter writer, Answer answer)
					throws IOException {

				// Log.d(TAG, "Serializing " +
				// answer.getQuestion().propertyName());
				//
				QuestionBase question = answer.getQuestion();
				//
				//
				// serializer.startTag("", QUESTION_TAG);
				//
				// Some questions have no properties
				// if(answer.getQuestion().propertyName()!=null)
				// {
				// writer.write(System.getProperty("line.separator"));
				// writer.write(question.propertyName() + ",");

				// serializer.attribute("", PROPERTY_ATTRIBUTE,
				// question.propertyName());
				// }
				//
				// // Some questions have no code
				// if (question.code() != null) {
				// serializer.attribute("", CODE_ATTRIBUTE,question.code());
				// }

				// writer.write(question.getCaption(org.africabridge.core.Survey.currentCulture())
				// + ",");
				// serializer.attribute("", LABEL_ATTRIBUTE,
				// question.getCaption(Survey.currentCulture()));
				//
				String values[] = answer.getValues();

				for (int i = 0; i < values.length; i++) {

					// serializer.startTag("", VALUE_TAG);

					String value = values[i];
					//
					if (value != null) {
						if (question instanceof TextSelectQuestion) {
							String str = ((TextSelectQuestion) question)
									.getOption(Survey.currentCulture(), value);

							// serializer.attribute("", ID_ATTRIBUTE, value);
							// serializer.text(str);
							writer.write(str + ",");
						} else {
							writer.write(value + ",");
						}
					}
				}

				writer.write(System.getProperty("line.separator"));
				//
				// serializer.endTag("", VALUE_TAG);
			}

			private void writeQuestion(XmlSerializer serializer, Answer answer)
					throws IOException {



				QuestionBase question = answer.getQuestion();

				serializer.startTag("", QUESTION_TAG);

				// Some questions have no properties
				if (answer.getQuestion().propertyName() != null) {
					serializer.attribute("", PROPERTY_ATTRIBUTE,
							question.propertyName());
				}

				// Some questions have no code
				if (question.code() != null) {
					serializer.attribute("", CODE_ATTRIBUTE, question.code());
				}

				serializer.attribute("", LABEL_ATTRIBUTE,
						question.getCaption(Survey.currentCulture()));

				String values[] = answer.getValues();
				for (int i = 0; i < values.length; i++) {

					serializer.startTag("", VALUE_TAG);

					String value = values[i];

					if (value != null) {
						if (question instanceof TextSelectQuestion) {
							String str = ((TextSelectQuestion) question)
									.getOption(Survey.currentCulture(), value);

							serializer.attribute("", ID_ATTRIBUTE, value);
							serializer.text(str);
						} else {
							serializer.text(value);
						}
					}

					serializer.endTag("", VALUE_TAG);
				}

				serializer.endTag("", QUESTION_TAG);
			}

			// Update the progress
			@Override
			protected void onProgressUpdate(Integer... values) {
				// set the current progress of the progress dialog
				progressDialog.setProgress(values[0]);
			}

			// after executing the code in the thread
			@Override
			protected void onPostExecute(Void result) {
				// close the progress dialog
				progressDialog.dismiss();

				if (error != null) {
					Toast.makeText(
							Export.this,
							"Export failed: "
									+ error.getClass().getSimpleName() + ": "
									+ error.getMessage(), Toast.LENGTH_LONG)
							.show();
				}
				bindSpinner();
			}
		}

		new ExportToXmlTask().execute();
	}
}
