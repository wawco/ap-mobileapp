package org.africabridge.core;

import org.africabridge.exceptions.ValidationException;
import org.africabridge.questions.QuestionBase;

public class Answer implements Comparable<Answer>
{

	private QuestionBase question;
	private String[] values;
	private boolean isSkipped;
	private boolean isDirty;
	private boolean isValid;
	
	public QuestionBase getQuestion() {
		return question;
	}
	
	public String[] getValues() {
		return values;
	}
	
	public String getValue(int index) {
	    if(index < values.length) {
		return values[index];
	    } else {
	        return null;
	    }
	}
	
	public boolean getIsValid() {
		return isValid;
	}

	public void setIsValid(boolean isValid) {
		this.isValid = isValid;
	}

	public void putValue(int index, String value)
	{
	    if(index < this.values.length) {
	        this.values[index] = value;
	    }
	}
	
	public void addValue(String value)
	{
		for(int i = 0; i< question.maxAnswers(); i++)
		{
			if(values[i]==null && i < values.length)
			{
				values[i] = value;
			}
		}
	}
	
	public void setIsSkipped(boolean isSkipped) {
		this.isSkipped = isSkipped;
	}

	public boolean getIsSkipped() {
		return isSkipped;
	}
	
	public boolean getIsDirty() {
		//TODO: Fix the dirty flag
		return true;
	}
	
	public void resetDirtyFlag()
	{
		isDirty = false;
	}
	
	public void Skip() throws ValidationException
	{
		throw new UnsupportedOperationException("Not implemented yet");
	}	
	
	public void Accept(String value) throws ValidationException
	{
		throw new UnsupportedOperationException("Not implemented yet");
	}	
	
	public Answer(QuestionBase question)
	{
		this.question = question;
		this.values = new String[question.maxAnswers()];
	}

	@Override
	public int compareTo(Answer other) {

		return this.getQuestion().id() - other.getQuestion().id();
		
	}
}
