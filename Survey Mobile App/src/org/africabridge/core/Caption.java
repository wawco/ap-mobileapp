package org.africabridge.core;

import java.security.spec.InvalidParameterSpecException;
import java.util.*;

import org.w3c.dom.*;

/**
 * Base class for objects that have localized caption strings
 * @author Team D
 *
 */
public abstract class Caption {
	

	protected final Map<Cultures, String> captions = new HashMap<Cultures, String>();
	
	
	
	/**
	 * Returns the caption based on the passed culture.
	 * @param culture
	 * @return caption string or null if there is no caption for the passed culture
	 */
	public String getCaption(Cultures culture) {
		if (captions.containsKey(culture))
			return captions.get(culture);

		return null;
	}
	
	
	/**
	 * Loads the captions from the selected node.
	 * @param captioNode - XML Node
	 * @throws InvalidParameterSpecException
	 * @throws DOMException
	 */
	protected void loadCaptions(Node captioNode) throws InvalidParameterSpecException, DOMException
	{
		Node node = captioNode.getFirstChild();
		
		while(node!=null)
		{
			if(node.getNodeName().equalsIgnoreCase("string"))
			{
				addCaptions((Element)node);
			}
			node = node.getNextSibling();
		}
	}
	
	/**
	 * Parses a caption string and adds it to the list of captions
	 * @param string - "string" XML Element
	 * @throws InvalidParameterSpecException
	 * @throws DOMException
	 */
	protected void addCaptions(Element string) throws InvalidParameterSpecException, DOMException
	{
		String value = string.getTextContent();
		
		Cultures culture = Cultures.parse(string.getAttribute("culture"));

		captions.put(culture, value.trim());
	}
}
