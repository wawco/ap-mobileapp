package org.africabridge.core;

import java.util.Date;

import org.africabridge.world.Location;

/**
 * Data class designed to hold survey summary
 * @author Team D
 *
 */
public final class SurveySummary {

	private long id;
	private int templateId;
	private int caseNumber;
	private String surveyTaker;
	private String supervisor;
	private Location location = new Location();
	private Date createdOn;
	private Date updatedOn;
	private Date exportedOn;

	private Boolean isCompleted;
	private Boolean isValid;

	public long getId() {
		return id;
	}

	public int getTemplateId() {
		return templateId;
	}

	public int getCaseNumber() {
		return caseNumber;
	}

	public String getSurveyTaker() {
		return surveyTaker;
	}

	public String getSupervisor() {
		return supervisor;
	}

	public Location getLocation() {
		return location;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public Date getExportedOn() {
		return exportedOn;
	}

	public Boolean getIsCompleted() {
		return isCompleted;
	}

	public Boolean getIsValid() {
		return isValid;
	}
	
	
	
	public String getFullNumber()
	{
		return String.format(
				"%02d-%02d-%03d",
				location.getWard().getId(),
				location.getVillage().getId(),
				caseNumber);
	}

	/**
	 * Creates a new instance of Survey Summary
	 * @param id
	 * @param templateId
	 * @param caseNumber
	 * @param surveyTaker
	 * @param supervisor
	 * @param location
	 * @param createdOn
	 * @param updatedOn
	 * @param exportedOn
	 * @param isCompleted
	 * @param isValid
	 */
	public SurveySummary(long id, int templateId, int caseNumber,
			String surveyTaker, String supervisor, Location location,
			Date createdOn, Date updatedOn, Date exportedOn,
			Boolean isCompleted, Boolean isValid) {
		super();
		this.id = id;
		this.templateId = templateId;
		this.caseNumber = caseNumber;
		this.surveyTaker = surveyTaker;
		this.supervisor = supervisor;
		this.location = location;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
		this.exportedOn = exportedOn;
		this.isCompleted = isCompleted;
		this.isValid = isValid;
	}

	public SurveySummary() {
		// TODO Auto-generated constructor stub
	}

}
