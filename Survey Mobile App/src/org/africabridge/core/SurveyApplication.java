package org.africabridge.core;

import java.util.Locale;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;
import android.text.TextUtils;

/**
 * Application override happening because we can't rely on the built-in 
 * locale handling.
 * 
 * @see http://www.roosmaa.net/application-wide-locale-override/
 *
 */
public class SurveyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        updateLanguage(this, null);
    }
    


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        updateLanguage(this, newConfig);
    }



    public static void updateLanguage(Context ctx, Configuration cfg) {
        // create a reference to preferences on the Survey
        Survey.setPreferences(PreferenceManager
                .getDefaultSharedPreferences(ctx));

//        Locale locale = new Locale(Survey.currentCulture().code());
//        Locale.setDefault(locale);

//        Configuration config = new Configuration();
//        config.locale = locale;
//        getResources().updateConfiguration(config, null);
//        SharedPreferences prefs = PreferenceManager
//                .getDefaultSharedPreferences(ctx);
//        String lang = prefs.getString("locale_override", "");
        String code = Survey.currentCulture().code();
        if (cfg == null) {
            cfg = new Configuration();
        }
        updateLanguage(ctx, cfg, Survey.currentCulture().code());
    }

    public static void updateLanguage(Context ctx, Configuration cfg, String lang) {
        if (!TextUtils.isEmpty(lang))
            cfg.locale = new Locale(lang);
        else
            cfg.locale = Locale.getDefault();

        ctx.getResources().updateConfiguration(cfg, null);
    }
}
