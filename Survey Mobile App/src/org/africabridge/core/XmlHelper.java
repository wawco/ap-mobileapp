package org.africabridge.core;

import org.w3c.dom.Node;


public final class XmlHelper {

	private XmlHelper() {
	}

	/**
	 * Searches for the first instance of with the provided name. Detaches the
	 * node form the document and returns it to the caller.
	 * 
	 * @param source
	 * @param name
	 * @return
	 */
	public static Node getChildNode(Node source, String name) {
		Node node = source.getFirstChild();

		while (node != null) {
			if (node.getNodeName().equalsIgnoreCase(name)) {
				node.getParentNode().removeChild(node);
				return node;
			}

			node = node.getNextSibling();
		}

		return null;
	}

	public static boolean hasChildNode(Node source, String name) {
		Node node = source.getFirstChild();

		while (node != null) {
			if (node.getNodeName().equalsIgnoreCase(name)) {
				return true;
			}

			node = node.getNextSibling();
		}

		return false;
	}
}
