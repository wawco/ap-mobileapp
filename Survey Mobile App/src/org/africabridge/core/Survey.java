package org.africabridge.core;

import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;

import org.africabridge.world.Location;
import org.africabridge.world.District;
import org.africabridge.world.Village;
import org.africabridge.world.Ward;

import android.content.SharedPreferences;

/**
 * This is the main survey driver class. It's primary role is to hold the survey
 * state, current settings and pointers to relevant objects.
 * 
 * @author Team D
 * 
 */
public class Survey {

	/* Application settings */

	private static District district;
	private static SharedPreferences preferences;

	private class PreferenceKeys {
		public static final String INSTALLATION_ID = "installationId";

		public static final String WARD = "ward";
		public static final String VILLAGE = "village";
		public static final String SUB_VILLAGE = "subVillage";

		public static final String LANGUAGE = "language";
		public static final String SURVEY_TAKER = "surveyTaker";
		public static final String SUPERVISOR = "supervisor";
		public static final String HIDE_EXPORTED = "hideExported";
	}

	public static void setPreferences(SharedPreferences preferences) {
		Survey.preferences = preferences;
	}

	/**
	 * Unique installation ID. Currently it's used on the export files to
	 * identify the originating device.
	 * 
	 * @return
	 */
	public static String installationId() {

		String installId = preferences.getString(
				PreferenceKeys.INSTALLATION_ID, null);

		if (installId == null) {
			installId = UUID.randomUUID().toString();

			SharedPreferences.Editor editor = preferences.edit();

			editor.putString(PreferenceKeys.INSTALLATION_ID, installId);
			editor.commit();
		}

		return installId;
	}

	public static void setRegion(District district) {
		Survey.district = district;
	}

	/**
	 * Current district
	 * 
	 * @return
	 */
	public static District district() {
		return district;
	}

	/**
	 * Current ward
	 * 
	 * @return current ward or Ward.Unknown if not set.
	 */
	public static Ward ward() {

		if (district != null)
			return district
					.getWard(preferences.getInt(PreferenceKeys.WARD, -1));

		return Ward.UNKNOWN;
	}

	/**
	 * Current village
	 * @return current village or Village.UNKNOWN when not set.
	 */
	public static Village village() {

		Ward ward = ward();

		if (ward != null)
			return ward.getVillage(preferences.getInt(PreferenceKeys.VILLAGE,
					-1));

		return Village.UNKNOWN;
	}

	/**
	 * Current subvillage
	 * @return sbvillage or an empty string if not set
	 */
	public static String subVillage() {
		return preferences.getString(PreferenceKeys.SUB_VILLAGE, "");
	}

	public static String surveyTaker() {
		return preferences.getString(PreferenceKeys.SURVEY_TAKER, "");
	}

	public static String supervisor() {
		return preferences.getString(PreferenceKeys.SUPERVISOR, "");
	}

	public static boolean hideExported() {
		return preferences.getBoolean(PreferenceKeys.HIDE_EXPORTED, false);
	}

	public static Location getCurrentLocation() {
		return new Location(district(), ward(), village(), subVillage(), 0, 0);
	}

	/**
	 * Sets the location 
	 * (will be used for all subsequent surveys and on the export activity)
	 * NOTE: Setting the location should happen as a single atomic operation
	 * 
	 * @param ward
	 *            - required
	 * @param village
	 *            - required
	 * @param subVillage
	 *            - optional
	 */
	public static void setLocation(Ward ward, Village village, String subVillage) {

		SharedPreferences.Editor editor = preferences.edit();

		editor.putInt(PreferenceKeys.WARD, ward.getId());
		editor.putInt(PreferenceKeys.VILLAGE, village.getId());
		editor.putString(PreferenceKeys.SUB_VILLAGE, subVillage);
		editor.commit();

	}

	/**
	 * Sets the preferences
	 * 
	 * @param language
	 * @param surevyTaker
	 * @param supervisor
	 * @param showExported
	 */
	public static void setPreferences(String language, String surevyTaker,
			String supervisor, boolean hideExported) {

		SharedPreferences.Editor ed = preferences.edit();

		ed.putInt(PreferenceKeys.LANGUAGE, Cultures.parse(language).id());

		ed.putString(PreferenceKeys.SURVEY_TAKER, surevyTaker);
		ed.putString(PreferenceKeys.SUPERVISOR, supervisor);

		ed.putBoolean(PreferenceKeys.HIDE_EXPORTED, hideExported);

		ed.commit();

	}

	private static Template template;

	/**
	 * Returns currently selected template.
	 * @return template
	 */
	public static Template getTemplate() {
		return template;
	}

	/**
	 * Sets the template for the current application.
	 * This will also affect the surveys that can get loaded on the home and export screens
	 * @param template
	 */
	public static void setTemplate(Template template) {
		if (Survey.template != null)
			throw new IllegalStateException(
					"Template can be set only once per application lifecycle");

		Survey.template = template;
	}

	/**
	 * Gets the current culture from the preferences
	 * @return
	 */
	public static Cultures currentCulture() {

		if (preferences != null) {

			Object o = preferences.getAll();
			return Cultures.getById(preferences.getInt(PreferenceKeys.LANGUAGE,
					Cultures.English.id()));
		}

		return Cultures.English;

	}

	/**
	 * 
	 * @return true if all settings are valid
	 */
	public static boolean areSettingValid() {

		return supervisor() != null && supervisor().trim().length() > 0
				&& surveyTaker() != null && surveyTaker().trim().length() > 0;

	}
}
