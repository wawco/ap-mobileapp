package org.africabridge.core;

import java.util.Date;

import org.africabridge.world.Location;

/**
 * A data class designed to hold survey item summary
 * similar to the SurveySummary.
 * @author Team D
 *
 */
public class ItemSummary {
	
	private int id;
	private int surveyId;
	private int caseNumber;
	private Date createdOn;
	private Date updatedOn;
	private Date exportedOn;
	
	private Boolean isCompleted;
	private Boolean isValid;
	public int getId() {
		return id;
	}
	public int getSurveyId() {
		return surveyId;
	}
	public int getCaseNumber() {
		return caseNumber;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public Date getExportedOn() {
		return exportedOn;
	}
	public Boolean getIsCompleted() {
		return isCompleted;
	}
	public Boolean getIsValid() {
		return isValid;
	}
	public ItemSummary(int id, int surveyId, int caseNumber,
			Date createdOn, Date updatedOn, Date exportedOn,
			Boolean isCompleted, Boolean isValid) {
		super();
		this.id = id;
		this.surveyId = surveyId;
		this.caseNumber = caseNumber;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
		this.exportedOn = exportedOn;
		this.isCompleted = isCompleted;
		this.isValid = isValid;
	}
	
	
}
