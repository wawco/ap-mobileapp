package org.africabridge.core;

import java.security.spec.InvalidParameterSpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.africabridge.exceptions.InvalidAnswersException;
import org.africabridge.questions.QuestionBase;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * Provides a way to group the questions into groups for the display.
 * @author Team D
 *
 */
public class QuestionGroup extends Caption {

	protected int id;
	protected String label;
	protected String jumpTo;
	protected Boolean valid;
	protected final ArrayList<QuestionBase> questions = new ArrayList<QuestionBase>();

	protected final Map<Cultures, String> captions = new HashMap<Cultures, String>();

	/**
	 * Group ID as set in the template
	 * @return
	 */
	public int id() {
		return id;
	}

	/**
	 * Group label (used for conditional jumps)
	 * @return string or null if note set
	 */
	public String label() {
		return label;
	}

	/**
	 * List of questions on this question group
	 * @return list of QuestionBase sorted by question ID
	 */
	@SuppressWarnings("unchecked")
	public List<QuestionBase> getQuestions() {
		return (List<QuestionBase>) questions.clone();
	}

	public Boolean isValid() {
		return valid;
	}
	
	public String jumpTo() {
		return jumpTo;
	}

	/**
	 * Creates an instance based on the passed XML node
	 * @param source - XML node
	 * @throws InvalidParameterSpecException
	 * @throws DOMException
	 * @throws IllegalArgumentException
	 */
	public QuestionGroup(Node source) throws InvalidParameterSpecException, DOMException, IllegalArgumentException {
		
		NamedNodeMap attributes = source.getAttributes();

		this.id = Integer.parseInt(attributes.getNamedItem("id").getNodeValue());
		
		Node temp = attributes.getNamedItem("label");
		
		if (temp != null)
			this.label = temp.getNodeValue();
		
		temp = attributes.getNamedItem("jumpTo");
		
		if (temp != null)
			this.jumpTo = temp.getNodeValue();
		
		this.loadCaptions(XmlHelper.getChildNode(source, "caption"));
		
		
		// load questions
		Node question = XmlHelper.getChildNode(source, "questions").getFirstChild();
		
		while(question!=null)
		{
			if(question.getNodeName().equalsIgnoreCase("question"))
			{
				questions.add(QuestionBase.CreateInstance(question));
			}
			question = question.getNextSibling();
		}
	}
}
