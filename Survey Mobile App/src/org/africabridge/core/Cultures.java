package org.africabridge.core;

public class Cultures {
	
	int id;
	String code;
	String name;
	
	/**
	 * Culture ID (arbitrarily assigned in the application)
	 * @return
	 */
	public int id() {
		return id;
	}
	
	/**
	 * Culture's ISO code
	 * @return
	 */
	public String code() {
		return code;
	}
	
	/**
	 * Culture name
	 * @return
	 */
	public String name() {
		return name;
	}
	
	protected Cultures(int id, String code, String name) {
		super();
		this.id = id;
		this.code = code;
		this.name = name;
	}
	
	public final static  Cultures English = new Cultures(1, "en", "English");
	public final static  Cultures Swahili = new Cultures(2, "sw", "Kiswahili");

	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return code;
	}
	
	/**
	 * Gets an instance of Cultures by its ID
	 * @param id
	 * @return instance of Cultures or null if an ID doesn't exist
	 */
	public static Cultures getById(int id)
	{
		if(id==2) return Swahili;
		
		return English;
	}
	
	/**
	 * Parses a string to an instance of Cultures
	 * @param value
	 * @return instance of Cultures or null if unsuccessful
	 */
	public static Cultures parse(String value)
	{
		if(value==null)
			return null;
		
		if(value.equalsIgnoreCase("en") || value.equalsIgnoreCase("English") || value.equalsIgnoreCase("Kiingereza")) 
		{
			return English;
		}
		
		if(value.equalsIgnoreCase("sw") || value.equalsIgnoreCase("Swahili") || value.equalsIgnoreCase("Kiswahili"))
		{
			return Swahili;
		}
		
		return null;
	}

	/**
	 * Returns localized names od all know cultures
	 * @return array of String
	 */
	public static String[] values()
	{
		return new String[] { English.name(), Swahili.name() };
	}
}
