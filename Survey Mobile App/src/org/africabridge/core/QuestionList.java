package org.africabridge.core;

import java.security.spec.InvalidParameterSpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.africabridge.questions.QuestionBase;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;


/**
 * Represents a survey section that contains a list of questions, caption etc.
 * Provides a way to get the questions either grouped or as a flat array list
 * @author Team D
 *
 */
public final class QuestionList extends Caption {

	private final ArrayList<QuestionGroup> groups = new ArrayList<QuestionGroup>();

	/**
	 * Returns a list of question groups sorted by the group ID
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<QuestionGroup> getGroups() {
		return (ArrayList<QuestionGroup>) groups.clone();
	}

	/**
	 * Returns a list of questions sorted by the question ID
	 * @return
	 */
	public ArrayList<QuestionBase> getQuestions()
	{
		ArrayList<QuestionBase> temp = new ArrayList<QuestionBase>();
		
		for(QuestionGroup group: this.groups)
		{
			temp.addAll(group.questions);
		}
		
		return temp;
	}
	
	


	/**
	 * Creates an instance of the QuestionList by parsing the passed XML Node
	 * @param source - XML node
	 * @throws InvalidParameterSpecException
	 * @throws DOMException
	 */
	public QuestionList(Node source) throws InvalidParameterSpecException, DOMException  {
		
		this.loadCaptions(XmlHelper.getChildNode(source, "caption"));
		this.loadGroups(XmlHelper.getChildNode(source, "groups"));
	}
	

	
	/**
	 * Loads question groups from the groups list XML Node
	 * @param groupsNode - XML Node containing a list of "group" nodes.
	 * @throws InvalidParameterSpecException
	 * @throws DOMException
	 */
	private void loadGroups(Node groupsNode) throws InvalidParameterSpecException, DOMException
	{
		Node group = groupsNode.getFirstChild();
		
		while(group!=null)
		{
			if(group.getNodeName().equalsIgnoreCase("group"))
			{
				groups.add(new QuestionGroup(group));
			}
			group = group.getNextSibling();
		}
		
	}
}
