package org.africabridge.core;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.africabridge.questions.QuestionBase;

import android.util.SparseArray;

public final class AnswerList {

	private final QuestionList questions;

	private final Stack<Integer> moveBackStack = new Stack<Integer>();
	private int index = 0;

	public AnswerList(QuestionList questions) {
		this.questions = questions;
		
		Collection<QuestionBase> temp = questions.getQuestions();
		
		for(QuestionBase question : temp)
		{
			Answer answer = new Answer(question);
			answers.put(question, answer);
			idMap.put(question.id(), answer);
		}
	}
	

	public String getCaption(Cultures culture) {
		return questions.getCaption(culture);
	}

	
	/* Answers */
	
	//The map is here to speed up question lookup. Each answer has a reference to it's questions, though.
	private final Map<QuestionBase, Answer> answers = new HashMap<QuestionBase, Answer>();
	private final SparseArray<Answer> idMap = new SparseArray<Answer>();
	
	public Answer getAnswer(QuestionBase question)
	{
		return answers.get(question);
	}
	
	public Answer getAnswer(int questionId)
	{
		return idMap.get(questionId);
	}
	
	public Answer[] getAllAnswers()
	{
		Answer[] temp = answers.values().toArray(new Answer[0]);
		
		Arrays.sort(temp);
		return temp;
	}
	
	
	/* Navigation */
	
	public QuestionGroup getGroup() {
		return questions.getGroups().get(index);
	}

	public QuestionGroup moveNext() {
		if (index >= (this.questions.getGroups().size() - 1)) {
			// we are at the end of the survey
			return null;
		}
		
		moveBackStack.push(index);

		index++;
		return getGroup();
	}

	public QuestionGroup movePrevious() {
		if (moveBackStack.isEmpty())
			return null;

		index = moveBackStack.pop();
		return getGroup();
	}
	
	/**
	 * Returns true or false based on whether we're at the start of the list of questions.
	 * @return boolean
	 */
	public boolean atStart() {
	    if (moveBackStack.isEmpty())
	        return true;
	    else return false;
	}
	
	public QuestionGroup jump(String label)
	{
		for(int i = index; i<this.questions.getGroups().size(); i++) //can't jump backwards, so start the current position
		{
			QuestionGroup g = questions.getGroups().get(i);
			if(g.label!=null && g.label.equalsIgnoreCase(label))
			{
				moveBackStack.push(index);
				index = i;
				return g;
			}
		}
		
		return null;
	}
}
