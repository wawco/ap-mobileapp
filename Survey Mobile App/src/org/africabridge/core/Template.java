package org.africabridge.core;

import java.security.spec.InvalidParameterSpecException;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * Survey template 
 * @author Team D - Team Dynomiiite!
 *
 */
public final class Template extends Caption {

	private int id;
	private String name;
	private final QuestionList headerQuestions;
	private final QuestionList itemQuestions;

	/* Methods */

	/**
	 * Survey ID
	 * @return
	 */
	public int id() {
		return id;
	}

	/**
	 * Survey name
	 * @return
	 */
	public String name() {
		return name;
	}

	/**
	 * 
	 * @return
	 */
	public QuestionList headerQuestions() {
		return headerQuestions;
	}

	public QuestionList itemQuestions() {
		return itemQuestions;
	}

	/* Loading functionality */

	/**
	 * Creates an instance of the Template from teh XMl document
	 * @param doc
	 * @throws InvalidParameterSpecException
	 * @throws DOMException
	 */
	public Template(Document doc) throws InvalidParameterSpecException, DOMException {

		Node root = doc.getDocumentElement();

		NamedNodeMap attributes = root.getAttributes();

		this.id = Integer.parseInt(attributes.getNamedItem("id").getNodeValue());
		this.name = attributes.getNamedItem("name").getNodeValue();

		this.headerQuestions = new QuestionList(XmlHelper.getChildNode(root, "header"));
		this.itemQuestions = new QuestionList(XmlHelper.getChildNode(root, "item"));

	}
	

}
