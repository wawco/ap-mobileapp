package org.africabridge.core;

/**
 * Dictionary keys 
 * @author Team D
 *
 */
public class BundleKeys
{
	public final static String MODE = "mode";
	public final static String SURVEY_ID = "surveyId";
	public final static String ITEM_ID = "itemId";
}
