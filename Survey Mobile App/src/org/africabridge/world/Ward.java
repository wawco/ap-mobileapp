package org.africabridge.world;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class Ward extends LocationElement{

	final Map<Integer, Village> villages = new HashMap<Integer, Village>();
	
	public final static Ward UNKNOWN = new Ward(0, "Unknown");
	
	public Ward(int id, String name)
	{
		this.id = id;
		this.name = name;
	}
	
	public Ward(Node source)
	{
		NamedNodeMap attributes = source.getAttributes();

		this.id = Integer.parseInt(attributes.getNamedItem("number").getNodeValue());
		this.name = attributes.getNamedItem("name").getNodeValue();
		
		Node node = source.getFirstChild();

		while (node != null) {
			if (node.getNodeName().equalsIgnoreCase("village")) {
				Village village = new Village(node);

				villages.put(village.getId(), village);
			}
			node = node.getNextSibling();
		}
		
	}	
	
	public Collection<Village> getVillages()
	{
		return villages.values();
	}
	
	public Village getVillage(int id)
	{
		if(villages.containsKey(id))
			return villages.get(id);
		
		return Village.UNKNOWN;
	}
}
