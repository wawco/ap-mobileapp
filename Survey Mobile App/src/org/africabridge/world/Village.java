package org.africabridge.world;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class Village extends LocationElement {

	final Map<Integer, Village> villages = new HashMap<Integer, Village>();
	
	public final static Village UNKNOWN = new Village(0, "Unknown");

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Village(int id, String name)
	{
		this.id = id;
		this.name = name;
	}
	
	public Village(Node node) {
		NamedNodeMap attributes = node.getAttributes();

		this.id = Integer.parseInt(attributes.getNamedItem("number")
				.getNodeValue());
		this.name = attributes.getNamedItem("name").getNodeValue();
	}

}
