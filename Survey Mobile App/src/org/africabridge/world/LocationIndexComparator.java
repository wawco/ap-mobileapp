package org.africabridge.world;

import java.lang.annotation.Target;
import java.util.Comparator;



public class LocationIndexComparator implements Comparator<LocationElement>
{
	@Override
	public int compare(LocationElement lhs, LocationElement rhs) {
		//Nice an concise and right from the private Integer.compare()
		return (lhs.id < rhs.id) ? -1 : ((lhs.id == rhs.id) ? 0 : 1);
	}	
}