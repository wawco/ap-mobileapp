package org.africabridge.world;

import java.util.Comparator;


public class LocationNameComparator implements Comparator<LocationElement> {

	@Override
	public int compare(LocationElement lhs, LocationElement rhs) {
		return lhs.getName().compareTo(rhs.getName());
	}

}
