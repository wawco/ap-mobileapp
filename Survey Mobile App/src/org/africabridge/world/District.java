package org.africabridge.world;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;


public class District extends LocationElement {

	// TODO: Support for multiple regions is still unimplemeneted
	// TODO: Region(Node node) {}
	
	final Map<Integer, Ward> wards = new HashMap<Integer, Ward>();

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public District(Node source) {
		NamedNodeMap attributes = source.getAttributes();

		this.id = Integer.parseInt(attributes.getNamedItem("number")
				.getNodeValue());
		this.name = attributes.getNamedItem("name").getNodeValue();

		Node node = source.getFirstChild();

		while (node != null) {
			if (node.getNodeName().equalsIgnoreCase("ward")) {
				Ward ward = new Ward(node);

				wards.put(ward.getId(), ward);
			}
			node = node.getNextSibling();
		}

	}

	public District(int regionId, String name) {
		this.id = regionId;
		this.name = name;
	}

	/**
	 * Returns non-empty wards (use getAllWards to get all wards)
	 * @return
	 */
	public Collection<Ward> getWards() {
		Collection<Ward> temp = new ArrayList<Ward>();

		for (Ward ward : wards.values()) {
			if (!ward.getVillages().isEmpty())
				temp.add(ward);
		}
		return temp;
	}

	public Collection<Ward> getAllWards() {
		return wards.values();
	}

	public Ward getWard(int id) {
		if (wards.containsKey(id))
			return wards.get(id);

		return Ward.UNKNOWN;
	}
}
