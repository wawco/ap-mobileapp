package org.africabridge.world;


public class Location {
	// TODO: Support for multiple regions is still unimplemeneted

	private District district;
	private Ward ward = Ward.UNKNOWN;
	private Village village = Village.UNKNOWN;
	private String subVillage;
	private float latitude;
	private float longitude;

	public District getDistrict() {
		return district;
	}

	public void setRegion(District district) {
		this.district = district;
	}

	public Ward getWard() {
		return ward;
	}

	public void setWard(Ward ward) {
		this.ward = ward;
	}

	public Village getVillage() {
		return village;
	}

	public void setVillage(Village village) {
		this.village = village;
	}

	public String getSubVillage() {
		return subVillage;
	}

	public void setSubVillage(String subVillage) {
		this.subVillage = subVillage;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public Location() {
	}

	public Location(District district, Ward ward, Village village,
			String subVillage, float latitude, float longitude) {
		super();
		this.district = district;
		this.ward = ward;
		this.village = village;
		this.subVillage = subVillage;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public boolean isValid() {
		
		return ward != null && ward != Ward.UNKNOWN && village!=null && village!=Village.UNKNOWN;
		
		
	}
}
