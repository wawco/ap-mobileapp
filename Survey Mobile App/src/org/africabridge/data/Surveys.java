/**
 * This class is the data access object for all survey related tables.
 * This means both question tables and survey tables.
 */

package org.africabridge.data;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.africabridge.core.Answer;
import org.africabridge.core.AnswerList;
import org.africabridge.core.ItemSummary;
import org.africabridge.core.QuestionList;
import org.africabridge.core.Survey;
import org.africabridge.core.SurveySummary;
import org.africabridge.exceptions.DataBaseException;
import org.africabridge.exceptions.InvalidStateException;
import org.africabridge.questions.QuestionBase;
import org.africabridge.world.Location;
import org.africabridge.world.LocationIndexComparator;
import org.africabridge.world.District;
import org.africabridge.world.Village;
import org.africabridge.world.Ward;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public final class Surveys {

	private static final String TAG = "Surveys";
	// Database fields

	private static OurSQLiteHelper dbHelper;

	public Surveys(Context context) {
		dbHelper = new OurSQLiteHelper(context);
	}

	public void open() throws SQLException {
		// SQLiteDatabase database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}
	
	/**
	 * Appends another condition to the passed SQL selection statement
	 * @param selection
	 * @param newCondition
	 * @return
	 */
	protected static String withCondition(String selection, String newCondition)
	{
		if(selection!=null && selection.length()>0)
		{
			return new String(selection + " AND " + newCondition);
			
		}
		
		return newCondition;
	}

	/* Servey summary lists */

	/**
	 * Gets a list of saved surveys based on the selected location
	 * @param ward - optional
	 * @param village - optional
	 * @param skipExported
	 * @return 
	 * @throws InvalidStateException
	 */
	public static List<SurveySummary> getSavedSurveys(Ward ward, Village village, boolean skipExported)
			throws InvalidStateException {

		SQLiteDatabase database = dbHelper.getReadableDatabase();
		
		String selection = null;
		
		if(skipExported) {
			selection = withCondition(selection, OurSQLiteHelper.COLUMN_EXPORTED + " < " + OurSQLiteHelper.COLUMN_UPDATED);
		}
		
		if(ward != null && !ward.equals(Ward.UNKNOWN))
		{
			selection = withCondition(selection, OurSQLiteHelper.COLUMN_LOC_WARD_ID + " = " + ward.getId());
		}
		
		if(village != null && !village.equals(Village.UNKNOWN))
		{
			selection = withCondition(selection, OurSQLiteHelper.COLUMN_LOC_VILLAGE_ID + " = " + village.getId());
		}

		List<SurveySummary> summaries = new ArrayList<SurveySummary>();
		Cursor cursor = database.query(OurSQLiteHelper.TABLE_SURVEYS, null,
				selection, null, null, null, null);
		cursor.moveToFirst();

		while (!cursor.isAfterLast()) {
			SurveySummary summary = cursorToSurveySummary(cursor);
			if (summary != null)
				summaries.add(summary);
			cursor.moveToNext();
		}
		cursor.close();
		database.close();
		return summaries;
	}

	/**
	 * Gets a list of saved surveys based on the selected location
	 * @param ward - optional
	 * @param village - optional
	 * @param skipExported
	 * @return 
	 * @throws InvalidStateException
	 */
	public static List<SurveySummary> getDirtySurveys(Ward ward, Village village, boolean skipIncomplete, boolean skipExported) throws InvalidStateException {

		SQLiteDatabase database = dbHelper.getReadableDatabase();

		String selection = null;
		
		if(skipExported) {
			selection = withCondition(selection, OurSQLiteHelper.COLUMN_EXPORTED + " < " + OurSQLiteHelper.COLUMN_UPDATED);
		}
		
		if(ward != null && !ward.equals(Ward.UNKNOWN))
		{
			selection = withCondition(selection, OurSQLiteHelper.COLUMN_LOC_WARD_ID + " = " + ward.getId());
		}
		
		if(village != null && !village.equals(Village.UNKNOWN))
		{
			selection = withCondition(selection, OurSQLiteHelper.COLUMN_LOC_VILLAGE_ID + " = " + village.getId());
		}

		if (skipIncomplete) {
			selection = withCondition(selection,  "NOT " + OurSQLiteHelper.COLUMN_COMPLETED + " = 0");
		}

		List<SurveySummary> summaries = new ArrayList<SurveySummary>();

		Cursor cursor = database.query(OurSQLiteHelper.TABLE_SURVEYS, null,
				selection, null, null, null, null);
		cursor.moveToFirst();

		while (!cursor.isAfterLast()) {
			SurveySummary summary = cursorToSurveySummary(cursor);

			if (summary != null)
				summaries.add(summary);
			cursor.moveToNext();
		}

		cursor.close();
		database.close();
		return summaries;
	}

	public static SurveySummary getSavedSurvey(long surveyId)
			throws InvalidStateException {

		SQLiteDatabase database = dbHelper.getReadableDatabase();

		String selection = new String(OurSQLiteHelper.COLUMN_SURVEY_ID + " = "
				+ surveyId);

		Cursor cursor = database.query(OurSQLiteHelper.TABLE_SURVEYS, null,
				selection, null, null, null, null);
		cursor.moveToFirst();

		SurveySummary retValue = null;

		while (!cursor.isAfterLast()) {
			retValue = cursorToSurveySummary(cursor);
			break;
		}
		cursor.close();
		database.close();
		return retValue;
	}

	/**
	 * Get a list of survey items based on the survey ID. Sill return all items,
	 * exported and not exported
	 * 
	 * @param surveyId
	 * @return
	 */
	public static List<ItemSummary> getSavedSurveyItems(long surveyId) {

		SQLiteDatabase database = dbHelper.getReadableDatabase();

		String selection = new String(OurSQLiteHelper.COLUMN_SURVEY_ID + " = "
				+ surveyId);

		List<ItemSummary> items = new ArrayList<ItemSummary>();
		Cursor cursor = database.query(OurSQLiteHelper.TABLE_ITEMS, null,
				selection, null, null, null, null);

		cursor.moveToFirst();

		while (!cursor.isAfterLast()) {
			ItemSummary item = cursorToItemSummary(cursor);
			items.add(item);
			cursor.moveToNext();
		}

		cursor.close();
		database.close();
		return items;
	}

	public static ItemSummary getSavedSurveyItem(long surveyId, long itemId) {

		SQLiteDatabase database = dbHelper.getReadableDatabase();

		String selection = new String(OurSQLiteHelper.COLUMN_SURVEY_ID + " = "
				+ surveyId + " AND " + OurSQLiteHelper.COLUMN_ITEM_ID + " = "
				+ itemId);

		Cursor cursor = database.query(OurSQLiteHelper.TABLE_ITEMS, null,
				selection, null, null, null, null);

		cursor.moveToFirst();

		ItemSummary retValue = null;

		while (!cursor.isAfterLast()) {
			retValue = cursorToItemSummary(cursor);
			break;
		}
		cursor.close();
		database.close();
		return retValue;
	}

	public static AnswerList getHeaderAnswers(long surveyId) {

		SQLiteDatabase database = dbHelper.getReadableDatabase();

		QuestionList questionList = Survey.getTemplate().headerQuestions();
		AnswerList answers = new AnswerList(questionList);

		String selection = OurSQLiteHelper.COLUMN_SURVEY_ID + " = " + surveyId
				+ " AND " + OurSQLiteHelper.COLUMN_ITEM_ID + " = "
				+ OurSQLiteHelper.NO_IID;
		Cursor cursor = database.query(OurSQLiteHelper.TABLE_ANSWERS, null,
				selection, null, null, null, null);
		cursor.moveToFirst();

		while (!cursor.isAfterLast()) {
			int questionId = cursor.getInt(cursor
					.getColumnIndex(OurSQLiteHelper.COLUMN_QUESTION_ID));
			Answer answer = answers.getAnswer(questionId);
			String answerText = cursor.getString(cursor
					.getColumnIndex(OurSQLiteHelper.COLUMN_VALUE));
			int answerIndex = cursor.getInt(cursor
					.getColumnIndex(OurSQLiteHelper.COLUMN_INDEX));

			// TODO: This an get item answers both should be refactored to avoid
			// all the duplicate
			// code
			Log.d(TAG, "answer from DB>>> QID: " + questionId + " val: "
					+ answerText);
			answer.putValue(answerIndex, answerText);
			cursor.moveToNext();
		}

		cursor.close();
		database.close();

		return answers;
	}
	

	public static AnswerList getItemAnswers(long surveyId, long itemId) {

		SQLiteDatabase database = dbHelper.getReadableDatabase();
		QuestionList questionList = Survey.getTemplate().itemQuestions();

		AnswerList answers = new AnswerList(questionList);

		String selection = OurSQLiteHelper.COLUMN_SURVEY_ID + " = " + surveyId
				+ " AND " + OurSQLiteHelper.COLUMN_ITEM_ID + " = " + itemId;
		Cursor cursor = database.query(OurSQLiteHelper.TABLE_ANSWERS, null,
				selection, null, null, null, null);
		cursor.moveToFirst();

		while (!cursor.isAfterLast()) {
			int questionId = cursor.getInt(cursor
					.getColumnIndex(OurSQLiteHelper.COLUMN_QUESTION_ID));
			Answer answer = answers.getAnswer(questionId);
			String answerText = cursor.getString(cursor
					.getColumnIndex(OurSQLiteHelper.COLUMN_VALUE));
			int answerIndex = cursor.getInt(cursor
					.getColumnIndex(OurSQLiteHelper.COLUMN_INDEX));



			// TODO: handle this a bit better

			if (answer != null)
				answer.putValue(answerIndex, answerText);
			cursor.moveToNext();
		}

		cursor.close();
		database.close();

		return answers;
	}

	
	
	

	public static long insertSurvey(Location location, int caseNumber,
			String surveyTaker, String supervisor) {


		// TODO: Save enough info to create an instance of SurveySummary (except
		// the name); Id = SerialNumber, so it should be between the min and max
		// preset

		SQLiteDatabase database = dbHelper.getWritableDatabase();

		String lid = new String();
		Ward ward = location.getWard();
		Village village = location.getVillage();
		District region = location.getDistrict();

		lid = lid + ward.getId() + region.getId() + village.getId();

		java.util.Date now = new java.util.Date();

		long insertId;
		ContentValues values = new ContentValues();
		// values.put(OurSQLiteHelper.COLUMN_LOCATION_ID, lid);
		values.put(OurSQLiteHelper.COLUMN_TEMPLATE_ID, Survey.getTemplate()
				.id());
		values.put(OurSQLiteHelper.COLUMN_CASE_NUMBER, caseNumber);
		values.put(OurSQLiteHelper.COLUMN_SURVEY_TAKER, surveyTaker);
		values.put(OurSQLiteHelper.COLUMN_SUPERVISOR, supervisor);

		values.put(OurSQLiteHelper.COLUMN_CREATED, now.getTime());
		values.put(OurSQLiteHelper.COLUMN_UPDATED, now.getTime());
		values.put(OurSQLiteHelper.COLUMN_EXPORTED, false);
		values.put(OurSQLiteHelper.COLUMN_COMPLETED, false);
		values.put(OurSQLiteHelper.COLUMN_IS_VALID, false);

		values.put(OurSQLiteHelper.COLUMN_LOC_REGION_ID, location.getDistrict()
				.getId());
		values.put(OurSQLiteHelper.COLUMN_LOC_WARD_ID, location.getWard()
				.getId());
		values.put(OurSQLiteHelper.COLUMN_LOC_VILLAGE_ID, location.getVillage()
				.getId());
		values.put(OurSQLiteHelper.COLUMN_LOC_SUBVILLAGE,
				location.getSubVillage());
		values.put(OurSQLiteHelper.COLUMN_LOC_LAT, location.getLatitude());
		values.put(OurSQLiteHelper.COLUMN_LOC_LON, location.getLongitude());

		insertId = database.insert(OurSQLiteHelper.TABLE_SURVEYS, null, values);
		database.close();

		return insertId; // return newly created ID
	}

	public static long insertSurveyItem(long surveyId, int caseNumber) {


		// TODO: Save enough info to create an instance of ItemSummary
		// Id = SerialNumber
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		java.util.Date now = new java.util.Date();
		long insertId;
		ContentValues values = new ContentValues();
		values.put(OurSQLiteHelper.COLUMN_CASE_NUMBER, caseNumber);
		values.put(OurSQLiteHelper.COLUMN_CREATED, now.getTime());
		values.put(OurSQLiteHelper.COLUMN_UPDATED, now.getTime());
		values.put(OurSQLiteHelper.COLUMN_EXPORTED, false);
		values.put(OurSQLiteHelper.COLUMN_COMPLETED, false);
		values.put(OurSQLiteHelper.COLUMN_IS_VALID, false);
		values.put(OurSQLiteHelper.COLUMN_SURVEY_ID, surveyId);

		insertId = database.insert(OurSQLiteHelper.TABLE_ITEMS, null, values);

		database.close();
		return insertId; // return newly created ID
	}

	public static void saveHeaderAnswer(long surveyId, int questionId,
			int answerIndex, String value, boolean isSkipped, boolean isValid) {
		Log.d(TAG, "saveHeaderAnswer called. SID: " + surveyId + "," + "QID: "
				+ questionId + ", " + "val: " + value + ", isSkipped: "
				+ isSkipped + ", isValid: " + isValid);
		SQLiteDatabase database = dbHelper.getWritableDatabase();
		long insertId;

		ContentValues values = new ContentValues();
		values.put(OurSQLiteHelper.COLUMN_SURVEY_ID, surveyId);

		values.put(OurSQLiteHelper.COLUMN_QUESTION_ID, questionId);
		values.put(OurSQLiteHelper.COLUMN_INDEX, answerIndex);
		values.put(OurSQLiteHelper.COLUMN_ITEM_ID, OurSQLiteHelper.NO_IID);
		values.put(OurSQLiteHelper.COLUMN_VALUE, value);
		values.put(OurSQLiteHelper.COLUMN_IS_SKIPPED, isSkipped);
		values.put(OurSQLiteHelper.COLUMN_IS_VALID, isValid);

		insertId = database.insertWithOnConflict(OurSQLiteHelper.TABLE_ANSWERS,
				null, values, SQLiteDatabase.CONFLICT_REPLACE);

		database.close();
	}

	public static void saveItemAnswer(long surveyId, long itemId,
			int questionId, int answerIndex, String value, boolean isSkipped,
			boolean isValid) throws SQLException {

		SQLiteDatabase database = dbHelper.getWritableDatabase();
		long insertId;
		ContentValues values = new ContentValues();
		values.put(OurSQLiteHelper.COLUMN_QUESTION_ID, questionId);
		values.put(OurSQLiteHelper.COLUMN_INDEX, answerIndex);
		values.put(OurSQLiteHelper.COLUMN_VALUE, value);
		values.put(OurSQLiteHelper.COLUMN_IS_SKIPPED, isSkipped);
		values.put(OurSQLiteHelper.COLUMN_IS_VALID, isValid);
		values.put(OurSQLiteHelper.COLUMN_SURVEY_ID, surveyId);
		values.put(OurSQLiteHelper.COLUMN_ITEM_ID, itemId);

		insertId = database.insertWithOnConflict(OurSQLiteHelper.TABLE_ANSWERS,
				null, values, SQLiteDatabase.CONFLICT_REPLACE);

		database.close();
	}

	public static int saveExportDate(long surveyId, java.util.Date date)
			throws SQLException {

		SQLiteDatabase database = dbHelper.getWritableDatabase();
		int count;
		ContentValues values = new ContentValues();
		values.put(OurSQLiteHelper.COLUMN_EXPORTED, date.getTime());
		String where = OurSQLiteHelper.COLUMN_SURVEY_ID + " = " + surveyId;

		count = database.update(OurSQLiteHelper.TABLE_SURVEYS, values, where,
				null);
		database.close();
		return count;
	}

	public static int saveValidFlag(long surveyId, boolean isValid) {

		SQLiteDatabase database = dbHelper.getWritableDatabase();
		int count;
		ContentValues values = new ContentValues();
		values.put(OurSQLiteHelper.COLUMN_IS_VALID, isValid);
		String where = OurSQLiteHelper.COLUMN_SURVEY_ID + " = " + surveyId;

		count = database.update(OurSQLiteHelper.TABLE_SURVEYS, values, where,
				null);
		database.close();
		return count;
	}
	
	public static int saveCompletedFlag(long surveyId, boolean isValid) {

		SQLiteDatabase database = dbHelper.getWritableDatabase();
		int count;
		ContentValues values = new ContentValues();
		values.put(OurSQLiteHelper.COLUMN_COMPLETED, isValid);
		String where = OurSQLiteHelper.COLUMN_SURVEY_ID + " = " + surveyId;

		count = database.update(OurSQLiteHelper.TABLE_SURVEYS, values, where,
				null);
		database.close();
		return count;
	}

	/* Cursor to value fiunctions */

	private static SurveySummary cursorToSurveySummary(Cursor cursor)
			throws InvalidStateException {

		// database = dbHelper.getWritableDatabase();
		SurveySummary temp = new SurveySummary(
				cursor.getInt(cursor
						.getColumnIndex(OurSQLiteHelper.COLUMN_SURVEY_ID)),
				cursor.getInt(cursor
						.getColumnIndex(OurSQLiteHelper.COLUMN_TEMPLATE_ID)),
				cursor.getInt(cursor
						.getColumnIndex(OurSQLiteHelper.COLUMN_CASE_NUMBER)),
				cursor.getString(cursor
						.getColumnIndex(OurSQLiteHelper.COLUMN_SURVEY_TAKER)),
				cursor.getString(cursor
						.getColumnIndex(OurSQLiteHelper.COLUMN_SUPERVISOR)),
				cursorToLocation(cursor),
				new Date(cursor.getLong(cursor
						.getColumnIndex(OurSQLiteHelper.COLUMN_CREATED))),
				new Date(cursor.getLong(cursor
						.getColumnIndex(OurSQLiteHelper.COLUMN_UPDATED))),
				new Date(cursor.getLong(cursor
						.getColumnIndex(OurSQLiteHelper.COLUMN_EXPORTED))),
				Boolean.valueOf(cursor.getInt(cursor
						.getColumnIndex(OurSQLiteHelper.COLUMN_COMPLETED)) > 0),
				Boolean.valueOf(cursor.getInt(cursor
						.getColumnIndex(OurSQLiteHelper.COLUMN_IS_VALID)) > 0));

		return temp;
	}

	private static ItemSummary cursorToItemSummary(Cursor cursor) {

		ItemSummary items = new ItemSummary(
				cursor.getInt(cursor
						.getColumnIndex(OurSQLiteHelper.COLUMN_ITEM_ID)),
				cursor.getInt(cursor
						.getColumnIndex(OurSQLiteHelper.COLUMN_SURVEY_ID)),
				cursor.getInt(cursor
						.getColumnIndex(OurSQLiteHelper.COLUMN_CASE_NUMBER)),
				new Date(cursor.getLong(cursor
						.getColumnIndex(OurSQLiteHelper.COLUMN_CREATED))),
				new Date(cursor.getLong(cursor
						.getColumnIndex(OurSQLiteHelper.COLUMN_UPDATED))),
				new Date(cursor.getLong(cursor
						.getColumnIndex(OurSQLiteHelper.COLUMN_EXPORTED))),
				Boolean.valueOf(cursor.getInt(cursor
						.getColumnIndex(OurSQLiteHelper.COLUMN_COMPLETED)) > 0),
				Boolean.valueOf(cursor.getInt(cursor
						.getColumnIndex(OurSQLiteHelper.COLUMN_IS_VALID)) > 0));
		return items;
	}

	public static Location cursorToLocation(Cursor cursor)
			throws InvalidStateException {
		int wardId = cursor.getInt(cursor
				.getColumnIndex(OurSQLiteHelper.COLUMN_LOC_WARD_ID));
		int villageId = cursor.getInt(cursor
				.getColumnIndex(OurSQLiteHelper.COLUMN_LOC_VILLAGE_ID));
		int regionId = cursor.getInt(cursor
				.getColumnIndex(OurSQLiteHelper.COLUMN_LOC_REGION_ID));
		String subVillage = cursor.getString(cursor
				.getColumnIndex(OurSQLiteHelper.COLUMN_LOC_SUBVILLAGE));
		int lat = cursor.getInt(cursor
				.getColumnIndex(OurSQLiteHelper.COLUMN_LOC_LAT));
		int lon = cursor.getInt(cursor
				.getColumnIndex(OurSQLiteHelper.COLUMN_LOC_LON));

		// Does this correspond to known regions?

		if (regionId != Survey.district().getId()) {
			throw new InvalidStateException();
		}

		Ward tempWard = Survey.district().getWard(wardId);
		Village tempVillage = tempWard.getVillage(villageId);

		// So far so good, let's glue it all together, then...

		Location temp = new Location(Survey.district(), tempWard, tempVillage,
				subVillage, lat, lon);

		// TODO: Uncomment this for the production code
		// if(!temp.isValid())
		// throw new InvalidStateException();

		return temp;
	}
	
	
	

}
