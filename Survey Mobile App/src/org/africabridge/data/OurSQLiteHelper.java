package org.africabridge.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class OurSQLiteHelper extends SQLiteOpenHelper {

	
	// Reference for the survey summary data
	// SurveySummary(int id,Location location,String name,
	// Date createdOn,Date updatedOn,Date exportedOn,Boolean isCompleted,Boolean isValid)
	
	public static final Integer NO_IID = -1;
	
	//Survey table
	public static final String TABLE_SURVEYS = "surveys";
	
	public static final String COLUMN_SURVEY_ID = "survey_id";  // Survey ID, key
	public static final String COLUMN_TEMPLATE_ID = "template_id"; //Template ID
	public static final String COLUMN_CASE_NUMBER = "case_number";
	public static final String COLUMN_SURVEY_TAKER = "survey_taker";
	public static final String COLUMN_SUPERVISOR = "supervisor";
	public static final String COLUMN_CREATED = "created";
	public static final String COLUMN_UPDATED = "updated";
	public static final String COLUMN_EXPORTED = "exported";
	public static final String COLUMN_COMPLETED = "complete";
	
	//public static final String COLUMN_IS_VALID = "valid";
	
	public static final String COLUMN_LOC_WARD_ID = "ward_id";
	public static final String COLUMN_LOC_REGION_ID = "region_id";
	public static final String COLUMN_LOC_VILLAGE_ID = "village_id";
	public static final String COLUMN_LOC_SUBVILLAGE = "subvillage";
	public static final String COLUMN_LOC_LAT = "latitude";
	public static final String COLUMN_LOC_LON = "longitude";
	
	
	//public static final String COLUMN_VERSION_MAJOR = "version_major";
	//public static final String COLUMN_VERSION_MINOR = "version_minor";
	
	// Answers table
	// Foregin key: surveys.sid
	public static final String TABLE_ANSWERS = "answers";
	public static final String COLUMN_QUESTION_ID = "question_id"; //Question ID
	public static final String COLUMN_INDEX = "indx"; //Question ID
	public static final String COLUMN_VALUE = "value";
	public static final String COLUMN_IS_SKIPPED = "skipped";
	//public static final String COLUMN_IID = "iid"; //Item ID
	public static final String COLUMN_IS_VALID = "is_valid";
	
	// Items table
	//(Date createdOn,Date updatedOn,Date exportedOn,Boolean isCompleted,Boolean isValid)
	public static final String TABLE_ITEMS = "items";
	public static final String COLUMN_ITEM_ID = "item_id";
	//public static final String COLUMN_SID = "sid";
	//public static final String COLUMN_NAME = "name";
	//public static final String COLUMN_LOCATION_ID = "lid";
	//public static final String COLUMN_CREATED = "created";
	//public static final String COLUMN_UPDATED = "updated";
	//public static final String COLUMN_EXPORTED = "exported"
	//public static final String COLUMN_COMPLETED = "complete";
	//public static final String COLUMN_IS_VALID = "valid";

	private static final String DATABASE_NAME = "surveys.db";
	private static final int DATABASE_VERSION = 4;
	
	private static final String DATABASE_CREATE_SURVEYS = 
			"CREATE TABLE IF NOT EXISTS " + TABLE_SURVEYS + "(" +
			COLUMN_SURVEY_ID + "  INTEGER PRIMARY KEY AUTOINCREMENT," +
			COLUMN_TEMPLATE_ID + "  INTEGER NOT NULL," +
			COLUMN_CASE_NUMBER + " INTEGER NOT NULL," +
			COLUMN_SURVEY_TAKER + " TEXT," +
			COLUMN_SUPERVISOR + " TEXT," +
			//COLUMN_NAME + " TEXT NOT NULL," +
			COLUMN_CREATED + " DATE NOT NULL," +
			COLUMN_UPDATED + " DATE NOT NULL," +
			COLUMN_EXPORTED + " DATE," +
			COLUMN_COMPLETED + " BOOLEAN NOT NULL," +
			COLUMN_IS_VALID + " BOOLEAN NOT NULL," +
			COLUMN_LOC_WARD_ID + " INTEGER," +
			COLUMN_LOC_REGION_ID + " INTEGER," +
			COLUMN_LOC_VILLAGE_ID + " INTEGER," +
			COLUMN_LOC_SUBVILLAGE + " TEXT," +
			COLUMN_LOC_LAT + " INTEGER," +
			COLUMN_LOC_LON + " INTEGER" +
			//"PRIMARY KEY (" + COLUMN_SID + "," + COLUMN_LOCATION_ID + ")" +
			")";
	private static final String DATABASE_CREATE_ANSWERS = 
			"CREATE TABLE IF NOT EXISTS " + TABLE_ANSWERS + " (" +
			COLUMN_QUESTION_ID + " INTEGER," +	
			COLUMN_INDEX + " INTEGER," +	
			COLUMN_VALUE + " TEXT," +
			COLUMN_ITEM_ID + " INTEGER DEFAULT NULL," +
			COLUMN_SURVEY_ID + " REFERENCES " + TABLE_SURVEYS + "(" + COLUMN_SURVEY_ID + ")," +
			COLUMN_IS_SKIPPED + " BOOLEAN NOT NULL," + 
			COLUMN_IS_VALID + " BOOLEAN," +
			"PRIMARY KEY (" + COLUMN_QUESTION_ID + "," + COLUMN_ITEM_ID + "," + COLUMN_INDEX + "," + COLUMN_SURVEY_ID + ")" +
			")";
	
	private static final String DATABASE_CREATE_ITEMS = 
			"CREATE TABLE IF NOT EXISTS " + TABLE_ITEMS + "(" +
			COLUMN_ITEM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +	
			//COLUMN_LOCATION_ID + " INTEGER," +
			COLUMN_SURVEY_ID + " REFERENCES " + TABLE_SURVEYS + "(" + COLUMN_SURVEY_ID + ")," +
			COLUMN_CASE_NUMBER + " INTEGER NOT NULL," +
			COLUMN_CREATED + " DATE NOT NULL," +
			COLUMN_UPDATED + " DATE NOT NULL," +
			COLUMN_EXPORTED + " DATE," +
			COLUMN_COMPLETED + " BOOLEAN NOT NULL," +
			COLUMN_IS_VALID + " BOOLEAN NOT NULL " +
			//"PRIMARY KEY (" + COLUMN_AID + "," + COLUMN_IID + ")" +
			")";

	private static final String TAG = "OurSQLiteHelper";
	
	public OurSQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE_SURVEYS);
		db.execSQL(DATABASE_CREATE_ITEMS);
		db.execSQL(DATABASE_CREATE_ANSWERS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ANSWERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEMS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SURVEYS);
        onCreate(db);
	}

}
