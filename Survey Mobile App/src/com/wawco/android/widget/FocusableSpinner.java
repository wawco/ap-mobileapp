package com.wawco.android.widget;

import android.content.Context;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;

/**
 * An Android Spinner that can receive focus, unlike regular spinners.
 *
 * @author Weston Wedding
 * @see     android.widget.Spinner
 */
public class FocusableSpinner extends Spinner implements OnTouchListener, OnItemSelectedListener {

    private boolean shouldPerformClick = false;  // Used in onTouch()
    private boolean initialSetup = true; // Used in onItemSelected

    /**
     * Basic constructor.
     *
     * @param context
     */
    public FocusableSpinner(Context context) {
        super(context);
        configureFocuses(context);
        this.setOnItemSelectedListener(this);
        this.setOnTouchListener(this);
    }

    /**
     * Sets this spinner's focusable attributes to true.
     * @param context
     */
    private void configureFocuses(Context context) {
        this.setFocusable(true);
        this.setFocusableInTouchMode(true);
        this.setNextFocusDownId(View.NO_ID);
    }

    /**
     * Implementation of OnItemSelectedListener.onItemSelected()
     * @see android.widget.AdapterView.OnItemSelectedListener#onItemSelected(android.widget.AdapterView, android.view.View, int, long)
     */
    @Override
    public void onItemSelected(AdapterView<?> parentView, View selectedView, int position,
            long id) {
        if(initialSetup) { // onItemSelected fires when the view is first created; we don't want to do anything yet
            initialSetup = false;
        } else {
            View view = parentView.focusSearch(View.FOCUS_DOWN);
            if(view != null) {
                view.requestFocus();
            }
        }
    }

    /**
     * Implementation of OnItemSelectedListener.onNothingSelected()
     *
     * @see android.widget.AdapterView.OnItemSelectedListener#onNothingSelected(android.widget.AdapterView)
     */
    @Override
    public void onNothingSelected(AdapterView<?> parentView) {
        // Do nothing
    }

    /**
     * Implementation of OnTouchListener.onTouch().
     *
     * When spinners are set to be focusable, the default click behavior is
     * to set the view as focused.  We want the spinner to open immediately
     * in addition to this, just like it did before it could gain focus.
     *
     * If the view is already focused and got clicked, the default behavior
     * is to open the select list.  We want to avoid performing two
     * performClick() calls here.
     *
     * @see android.view.View.OnTouchListener#onTouch(android.view.View, android.view.MotionEvent)
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {

        boolean defaultResponse = v.onTouchEvent(event);

        switch(event.getActionMasked()) {

            case MotionEvent.ACTION_DOWN:
                if(!v.hasFocus()) {
                    shouldPerformClick = true;
                }
                return defaultResponse;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_OUTSIDE:
                if(shouldPerformClick && v.hasFocus()) {
                    v.performClick();
                }
                shouldPerformClick = false;
                break;
            default:
                return defaultResponse;
        }

        return true;  // If we got here, we ate the event
    }

    @Override
    protected void onFocusChanged(boolean gainFocus, int direction,
            Rect previouslyFocusedRect) {
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
        if(gainFocus) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


}
